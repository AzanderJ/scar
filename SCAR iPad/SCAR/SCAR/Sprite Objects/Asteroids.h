//
//  Asteroids.h
//  SCAR
//
//  Created by Alexzander Jaggi on 3/22/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Asteroids : SKSpriteNode

+(Asteroids*)initAsteroids:(SKScene *)theScene;
-(void)moveAsteroids: (SKScene *)theScene;

@end
