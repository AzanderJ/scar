//
//  DefenseDelegate.h
//  SCAR
//
//  Created by Alexzander Jaggi on 4/14/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSUserDefaults+RMSaveCustomObject.h"
#import "FirebaseData.h"
#import "Ships.h"
#import "DefenseEquipment.h"
#import "SystemEquipment.h"
#import "DefenseTab.h"
#import "TopMenu.h"
#import "GameScene.h"

@interface DefenseDelegate : NSObject <UITableViewDelegate, UITableViewDataSource>

+(DefenseDelegate *)initDefenseDelegateWithShip:(Ships *)theShip theAvalibleDefense:(NSArray *)theAvalibleDefense theScene:(SKScene *)theScene;
+(void)savedClicked;
+(void)loadClicked;
+(void)deleteClicked;
+(void)simClicked;
+(void)stopSim;

@end
