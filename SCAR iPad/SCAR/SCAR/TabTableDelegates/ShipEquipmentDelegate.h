//
//  ShipEquipmentDelegate.h
//  SCAR
//
//  Created by Alexzander Jaggi on 4/10/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NSUserDefaults+RMSaveCustomObject.h"
#import "FirebaseData.h"
#import "Ships.h"
#import "Weapons.h"
#import "DefenseEquipment.h"
#import "WeaponTab.h"
#import "TopMenu.h"
#import "GameScene.h"
#import "EquipmentView.h"


@interface ShipEquipmentDelegate : NSObject <UITableViewDelegate, UITableViewDataSource>

+(ShipEquipmentDelegate *)initShipMountDelegateWithShip:(Ships *)theShip theWeapons:(NSArray *)theWeapons theFixedWep:(NSArray *)fixedArray theGimbalArray:(NSArray *)gimbalArray theAvalibleDefense:(NSArray *)theAvalibleDefense theScene:(SKScene *)theScene;
+(void)savedClicked;
+(void)loadClicked;
+(void)deleteClicked;
+(void)simClicked;
+(void)stopSim;
+(NSNumber *)getDPS;
@end
