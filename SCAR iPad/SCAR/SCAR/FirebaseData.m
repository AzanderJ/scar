//
//  FirebaseData.m
//  SCAR
//
//  Created by Alexzander Jaggi on 4/3/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import "FirebaseData.h"

@implementation FirebaseData

+(NSMutableArray *)getShipList{
    NSMutableArray * ships = [[NSMutableArray alloc]init];
    Firebase * firebaseLink = [[Firebase alloc]initWithUrl:@"https://scar.firebaseio.com/ships"];
    [firebaseLink keepSynced:YES];
    [firebaseLink observeEventType:FEventTypeValue withBlock:^(FDataSnapshot * snapshot){
        for (FDataSnapshot * child in snapshot.children){
            
            
            Ships * shipObject = [[Ships alloc]init];
            shipObject.shipModel = child.key;
            shipObject.shipBrand = child.value[@"Brand"];
            shipObject.shipDescription = child.value[@"Description"];
            shipObject.shipMass = child.value[@"Mass"];
            shipObject.shipCargoCap = child.value[@"Cargo"];
            shipObject.shipFocus = child.value[@"Focus"];
            shipObject.shipMaxCrew = child.value[@"Crew"];
            shipObject.shipLength = child.value[@"Length"];
            shipObject.shipFixedAmount = child.value[@"Fixed Amount"];
            shipObject.shipFixedSize = child.value[@"Fixed Size"];
            shipObject.shipGimbaledAmount = child.value[@"Gimbal Amount"];
            shipObject.shipGimbalSize = child.value[@"Gimbal Size"];
            shipObject.shipShield = child.value[@"Shield"];
            shipObject.shipIMG = child.value[@"Image"];
            
            [ships addObject:shipObject];
        };
    }];
    
    return ships;
}

+(NSMutableArray *)getShipWeaponsFixedMaxSize:(NSString *)fixedMaxSize gimbalMaxSize:(NSString *)gimbalMaxSize{
    
    int fixedSize = [fixedMaxSize intValue];
    int gimbalSize = [gimbalMaxSize intValue];
    int maxSize;
    if (fixedSize < gimbalSize) {
        maxSize = gimbalSize;
    }else{
        maxSize = fixedSize;
    }
    NSMutableArray * weaponsList = [[NSMutableArray alloc]init];
    
    Firebase * firebaseLink = [[Firebase alloc]initWithUrl:@"https://scar.firebaseio.com/weapons"];
    [firebaseLink keepSynced:YES];
    [[firebaseLink queryOrderedByChild:@"Size"] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot * snapshot){
        for (FDataSnapshot * child in snapshot.children){
            if ([child.value[@"Size"]integerValue] <= maxSize) {
                Weapons * weapon = [[Weapons alloc]init];
                weapon.wepName = child.key;
                weapon.wepDPS = child.value[@"DPS"];
                weapon.wepFireRate = child.value[@"Fire Rate"];
                weapon.wepManufacturer = child.value[@"Maker"];
                weapon.wepRange = child.value[@"Range"];
                weapon.wepSize = child.value[@"Size"];
                weapon.wepSpeed = child.value[@"Speed"];
                weapon.wepType = child.value[@"Type"];
                [weaponsList addObject:weapon];
            };
        };
    }];
    return weaponsList;
}
+(NSMutableArray *)getShipWeaponsSingleType:(NSString *)theSize{
    
    NSMutableArray * weaponsList = [[NSMutableArray alloc]init];
    
    Firebase * firebaseLink = [[Firebase alloc]initWithUrl:@"https://scar.firebaseio.com/weapons"];
    [firebaseLink keepSynced:YES];
    [[firebaseLink queryOrderedByChild:@"Size"] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot * snapshot){
        for (FDataSnapshot * child in snapshot.children){
            if ([child.value[@"Size"]integerValue] <= [theSize integerValue]) {
                Weapons * weapon = [[Weapons alloc]init];
                weapon.wepName = child.key;
                weapon.wepDPS = child.value[@"DPS"];
                weapon.wepFireRate = child.value[@"Fire Rate"];
                weapon.wepManufacturer = child.value[@"Maker"];
                weapon.wepRange = child.value[@"Range"];
                weapon.wepSize = child.value[@"Size"];
                weapon.wepSpeed = child.value[@"Speed"];
                weapon.wepType = child.value[@"Type"];
                [weaponsList addObject:weapon];
            };
        };
    }];
    
    return weaponsList;
}

+(NSArray *)getDefenseEquipmentBySize:(NSString *)defenseSize{
    NSMutableArray * defenseEquipList2 = [[NSMutableArray alloc]init];
    
    Firebase * firebaseLink = [[Firebase alloc]initWithUrl:@"https://scar.firebaseio.com/Defense/Shields"];
    [firebaseLink keepSynced:YES];
    [[firebaseLink queryOrderedByChild:@"Size"] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot * snapshot){
        for (FDataSnapshot * child in snapshot.children){
            if ([child.value[@"Size"]integerValue] == [defenseSize integerValue]) {
                DefenseEquipment * defEquip = [[DefenseEquipment alloc]init];
                defEquip.defenseName = child.key;
                defEquip.defenseDescription = child.value[@"Description"];
                defEquip.defenseHP = child.value[@"HP"];
                defEquip.defenseRegenRate = child.value[@"Regen Rate"];
                defEquip.defenseSize = child.value[@"Size"];
                [defenseEquipList2 addObject:defEquip];
            };
        };
    }];
    return defenseEquipList2;
}


+(NSDictionary *)getSystemEquipment{
    NSDictionary * equip = [[NSDictionary alloc]init];
    NSMutableArray * coolerList = [[NSMutableArray alloc]init];
    NSMutableArray * powerList = [[NSMutableArray alloc]init];
    Firebase * firebaseLinkCoolers = [[Firebase alloc]initWithUrl:@"https://scar.firebaseio.com/Systems/Coolers"];
    [firebaseLinkCoolers keepSynced:YES];
    [firebaseLinkCoolers observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        for (FDataSnapshot * child in snapshot.children) {
            SystemEquipment * sysEquip = [[SystemEquipment alloc]init];
            sysEquip.itemName = child.key;
            sysEquip.itemDescription = child.value[@"Description"];
            sysEquip.itemAblility = [NSString stringWithFormat:@"Cooling: %@",child.value[@"Cooling"]];
            [coolerList addObject:sysEquip];
        };
    }];
    Firebase * firebaseLinkPower = [[Firebase alloc]initWithUrl:@"https://scar.firebaseio.com/Systems/Power Plants"];
    [firebaseLinkPower keepSynced:YES];
    [firebaseLinkPower observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        for (FDataSnapshot * child in snapshot.children) {
            SystemEquipment * sysEquip = [[SystemEquipment alloc]init];
            sysEquip.itemName = child.key;
            sysEquip.itemDescription = child.value[@"Description"];
            sysEquip.itemAblility = [NSString stringWithFormat:@"Power Generation: %@", child.value[@"Power Generation"]];
            [powerList addObject:sysEquip];
        };
    }];
    
    equip = @{@"coolers":coolerList, @"power":powerList};
    
    return equip;
}

@end
