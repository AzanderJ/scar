//
//  main.m
//  SCAR
//
//  Created by Alexzander Jaggi on 3/22/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
