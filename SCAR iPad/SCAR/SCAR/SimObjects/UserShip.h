//
//  UserShip.h
//  SCAR
//
//  Created by Alexzander Jaggi on 4/17/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Ships.h"

@interface UserShip : SKSpriteNode

+(UserShip *) initNewUserShip:(SKScene *)theScene theShip:(Ships *)theShip;

@end
