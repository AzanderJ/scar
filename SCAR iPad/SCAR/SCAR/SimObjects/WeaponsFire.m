//
//  WeaponsFire.m
//  SCAR
//
//  Created by Alexzander Jaggi on 4/17/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import "WeaponsFire.h"

@implementation WeaponsFire

WeaponsFire * theBullet;
SKScene * theGameScene;

+(WeaponsFire *)initFire:(SKScene *)theScene startingPoint:(CGPoint)location{
    theGameScene = theScene;
    theBullet = [WeaponsFire spriteNodeWithImageNamed:@"weaponsFire"];
    theBullet.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:theBullet.frame.size.height/2];
    theBullet.physicsBody.friction = 0;
    theBullet.zPosition = 1;
    theBullet.xScale = 0.4;
    theBullet.yScale = 0.4;
    theBullet.position = location;
    theBullet.physicsBody.mass =0;
    theBullet.hidden = NO;
    [theScene addChild:theBullet];
    
    return theBullet;
}
+(void)fireWeapon:(int)whatShip{
    CGVector fire;
    SKAction * fireWeaponSound = [SKAction playSoundFileNamed:@"Laser_01.wav" waitForCompletion:NO];
    if ([GameScene getAudioPOS] == 0) {
        [theGameScene runAction:fireWeaponSound];
    }
    if (whatShip == 0) {
        fire = CGVectorMake(600, 0);
        [theBullet.physicsBody applyImpulse:fire];
        

    }
    if (whatShip == 1) {
        fire = CGVectorMake(-600, 0);
        [theBullet.physicsBody applyImpulse:fire];
    }
}
@end
