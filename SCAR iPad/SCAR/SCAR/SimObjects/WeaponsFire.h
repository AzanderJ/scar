//
//  WeaponsFire.h
//  SCAR
//
//  Created by Alexzander Jaggi on 4/17/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "GameScene.h"

@interface WeaponsFire : SKSpriteNode

+(WeaponsFire *)initFire:(SKScene *)theScene startingPoint:(CGPoint)location;
+(void)fireWeapon:(int)whatShip;

@end
