//
//  Weapons.h
//  SCAR
//
//  Created by Alexzander Jaggi on 3/23/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+RMArchivable.h"
@interface Weapons : NSObject

@property(nonatomic) NSString * wepName;
@property(nonatomic) NSString * wepDPS;
@property(nonatomic) NSString * wepFireRate;
@property(nonatomic) NSString * wepManufacturer;
@property(nonatomic) NSString * wepRange;
@property(nonatomic) NSString * wepSize;
@property(nonatomic) NSString * wepSpeed;
@property(nonatomic) NSString * wepType;

@end
