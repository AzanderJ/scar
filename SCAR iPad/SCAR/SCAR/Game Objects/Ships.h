//
//  Ships.h
//  SCAR
//
//  Created by Alexzander Jaggi on 3/23/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Ships : NSObject

@property (nonatomic) NSString * shipBrand;
@property (nonatomic) NSString * shipModel;
@property (nonatomic) NSString * shipIMG;
@property (nonatomic) NSString * shipDescription;
@property (nonatomic) NSString * shipMass;
@property (nonatomic) NSString * shipCargoCap;
@property (nonatomic) NSString * shipFocus;
@property (nonatomic) NSString * shipMaxCrew;
@property (nonatomic) NSString * shipLength;
@property (nonatomic) NSString * shipFixedAmount;
@property (nonatomic) NSString * shipFixedSize;
@property (nonatomic) NSString * shipGimbaledAmount;
@property (nonatomic) NSString * shipGimbalSize;
@property (nonatomic) NSString * shipShield;
@property (nonatomic) NSDictionary * savedConfig;

@end
