//
//  DefenseEquipment.h
//  SCAR
//
//  Created by Alexzander Jaggi on 4/8/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DefenseEquipment : NSObject

@property (nonatomic) NSString * defenseName;
@property (nonatomic) NSString * defenseDescription;
@property (nonatomic) NSString * defenseHP;
@property (nonatomic) NSString * defenseRegenRate;
@property (nonatomic) NSString * defenseSize;


@end
