//
//  RedditView.h
//  SCAR
//
//  Created by Alexzander Jaggi on 3/24/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import <UIKit/UIKit.h>
#import "RedditPosts.h"
#import "GameScene.h"

@interface RedditView : NSObject <UITableViewDelegate, UITableViewDataSource, UIWebViewDelegate>

+(void)openRedditView:(SKScene *)theScene;
+(void)closeRedditView;
+(void)doneTouched;

@end
