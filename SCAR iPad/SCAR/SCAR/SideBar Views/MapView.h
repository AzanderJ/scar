//
//  MapView.h
//  SCAR
//
//  Created by Alexzander Jaggi on 3/24/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import <UIKit/UIKit.h>

@interface MapView : NSObject <UIWebViewDelegate>



+(void)openMapView:(SKScene *)theScene;
+(void)closeMapView;
+(void)activateMap:(int)whatMap;

@end
