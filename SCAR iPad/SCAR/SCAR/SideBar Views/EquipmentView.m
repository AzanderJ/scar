//
//  EquipmentView.m
//  SCAR
//
//  Created by Alexzander Jaggi on 3/23/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import "EquipmentView.h"

@implementation EquipmentView

SKSpriteNode * backgroundNodeEquip;
SKSpriteNode * backgroundMid;
SKSpriteNode * backgroundBottom;
SKSpriteNode * generalTab;
SKSpriteNode * weaponsTab;
SKSpriteNode * defenseTab;
SKSpriteNode * systemTab;
SKSpriteNode * scanLine;
SKScene * mainScene;
SKAction * runScanSeq;
SKAction * scan;
NSArray * weaponsAll;
NSArray * weaponsArrayFixed;
NSArray * weaponsArrayGimbal;
NSArray * defenseArray;
NSDictionary * systemArray;
NSArray * backgroundBottomTextures;
NSArray * backgroundMidTextures;
SKEmitterNode * emitterEffect;
int isPresented = 0;

Ships * theShip;

+(void)openEquipmentView:(SKScene *)theScene{
    
    mainScene = theScene;
    
    //view background
    backgroundNodeEquip = [SKSpriteNode spriteNodeWithImageNamed:@"backgroundTop"];
    backgroundNodeEquip.zPosition = 2;
    backgroundNodeEquip.alpha = 0.75;
    backgroundNodeEquip.size = CGSizeMake(theScene.frame.size.width/1.20, theScene.frame.size.height/1.22);
    backgroundNodeEquip.position = CGPointMake(theScene.size.width - backgroundNodeEquip.size.width/1.85, theScene.size.height/2.3);
    
    NSMutableArray * midFrames = [[NSMutableArray alloc]init];
    SKTextureAtlas * midTextureAtlas = [SKTextureAtlas atlasNamed:@"mid"];
    long midTextureCount = midTextureAtlas.textureNames.count;
    for (int i=1; i <= midTextureCount; i++) {
        NSString * midTextureName = [NSString stringWithFormat:@"midLayer%d",i];
        SKTexture * midTempTexture = [midTextureAtlas textureNamed:midTextureName];
        [midFrames addObject:midTempTexture];
    }
    backgroundMidTextures = [[NSArray alloc]initWithArray:midFrames];
    
    backgroundMid = [SKSpriteNode spriteNodeWithImageNamed:@"midLayer1"];
    backgroundMid.size = CGSizeMake(theScene.frame.size.width/1.20, theScene.frame.size.height/1.22);
    backgroundMid.position = CGPointMake(theScene.size.width - backgroundNodeEquip.size.width/1.85, theScene.size.height/2.3);
    backgroundMid.zPosition = 1;
    backgroundMid.alpha = 0.3;
    
    NSMutableArray * bottomFrames = [[NSMutableArray alloc]init];
    SKTextureAtlas * bottomTextureAtlas = [SKTextureAtlas atlasNamed:@"bottom"];
    long bottomTextureCount = bottomTextureAtlas.textureNames.count;
    for (int j=1; j <= bottomTextureCount; j++) {
        NSString * bottomTextureName = [NSString stringWithFormat:@"bottomLayer%d",j];
        SKTexture * bottomTempTexture = [bottomTextureAtlas textureNamed:bottomTextureName];
        [bottomFrames addObject:bottomTempTexture];
    }
    backgroundBottomTextures = [[NSArray alloc]initWithArray:bottomFrames];
    
    backgroundBottom = [SKSpriteNode spriteNodeWithTexture:bottomFrames[0]];
    backgroundBottom.size = CGSizeMake(theScene.frame.size.width/1.20, theScene.frame.size.height/1.22);
    backgroundBottom.position = CGPointMake(theScene.size.width - backgroundNodeEquip.size.width/1.85, theScene.size.height/2.3);
    backgroundBottom.zPosition = 0;
    backgroundBottom.alpha = 0.5;
    
    //Scan Line & dust effect
    scanLine = [SKSpriteNode spriteNodeWithImageNamed:@"scanLine"];
    scanLine.zPosition = 0;
    scanLine.alpha = 0.2;
    scanLine.size = CGSizeMake(backgroundNodeEquip.frame.size.width, scanLine.frame.size.height/20);
    scanLine.position = CGPointMake(backgroundNodeEquip.frame.size.width/1.52, backgroundNodeEquip.frame.size.height/50);
    scanLine.hidden = FALSE;
    
    scan = [SKAction moveTo:CGPointMake(backgroundNodeEquip.frame.size.width/1.52, backgroundNodeEquip.frame.size.height) duration:5];
    SKAction * resetScan = [SKAction moveTo:CGPointMake(backgroundNodeEquip.frame.size.width/1.52, backgroundNodeEquip.frame.size.height/50) duration:0];
    runScanSeq = [SKAction sequence:@[scan,resetScan]];
    
    emitterEffect = [SKEmitterNode nodeWithFileNamed:@"ScanParticals"];
    emitterEffect.particlePositionRange = CGVectorMake(backgroundNodeEquip.frame.size.width, backgroundNodeEquip.frame.size.height/1.5);
    emitterEffect.zPosition = 0;
    
    float screenSize =  backgroundNodeEquip.size.width/1.25;

    //Tabs
    generalTab = [SKSpriteNode spriteNodeWithImageNamed:@"generalInactive"];
    generalTab.size = CGSizeMake(backgroundNodeEquip.size.width/5, backgroundNodeEquip.size.height/12);
    generalTab.zPosition = 2;
    generalTab.alpha  = 1.25;
    generalTab.position = CGPointMake(backgroundNodeEquip.size.width/2.4 - screenSize, backgroundNodeEquip.size.height/1.89);
    generalTab.name = @"generalTab";
    
    weaponsTab = [SKSpriteNode spriteNodeWithImageNamed:@"weaponsInactive"];
    weaponsTab.size = CGSizeMake(backgroundNodeEquip.size.width/5, backgroundNodeEquip.size.height/12);
    weaponsTab.zPosition = 2;
    weaponsTab.alpha = 1.25;
    weaponsTab.position = CGPointMake(backgroundNodeEquip.size.width/1.63 - screenSize, backgroundNodeEquip.size.height/1.89);
    weaponsTab.name = @"weaponsTab";
    
    defenseTab = [SKSpriteNode spriteNodeWithImageNamed:@"defenseInactive"];
    defenseTab.size = CGSizeMake(backgroundNodeEquip.size.width/5, backgroundNodeEquip.size.height/12);
    defenseTab.zPosition = 2;
    defenseTab.alpha = 1.25;
    defenseTab.position = CGPointMake(backgroundNodeEquip.size.width/1.234 - screenSize, backgroundNodeEquip.size.height/1.89);
    defenseTab.name = @"defenseTab";
    
    systemTab = [SKSpriteNode spriteNodeWithImageNamed:@"systemsInactive"];
    systemTab.size = CGSizeMake(backgroundNodeEquip.size.width/5, backgroundNodeEquip.size.height/12);
    systemTab.zPosition = 2;
    systemTab.alpha = 1.25;
    systemTab.position = CGPointMake(backgroundNodeEquip.size.width/0.993 - screenSize, backgroundNodeEquip.size.height/1.89);
    systemTab.name = @"systemTab";

    
    //--------------///-----------------//
    
    [TopMenu presentTopMenu:mainScene viewBackground:backgroundNodeEquip];
}

//called when ship is selected from topMenu
+(void)presentDataViews:(Ships *)ship{
    
    theShip = ship;
    [GameScene setShipSelectorPOS:0];
    weaponsAll = [FirebaseData getShipWeaponsFixedMaxSize:theShip.shipFixedSize gimbalMaxSize:theShip.shipGimbalSize];
    weaponsArrayFixed = [FirebaseData getShipWeaponsSingleType:ship.shipFixedSize];
    weaponsArrayGimbal = [FirebaseData getShipWeaponsSingleType:ship.shipGimbalSize];
    defenseArray = [FirebaseData getDefenseEquipmentBySize:ship.shipShield];
    systemArray = [FirebaseData getSystemEquipment];
    SKAction * clickedSound = [SKAction playSoundFileNamed:@"click2.wav" waitForCompletion:NO];

    
    switch (isPresented) {
            
        case 0:
            isPresented = 1;
            [GameScene tab1POS:1];
            [GameScene tab2POS:0];
            [GameScene tab3POS:0];
            [GameScene tab4POS:0];
            [self deactivateWeaponTab];
            [self deactivateDefenseTab];
            [self deactivateSystemTab];
            if ([GameScene getAudioPOS] == 0) {
                [mainScene runAction:clickedSound];
            }
            
            [mainScene addChild:backgroundBottom];
            [mainScene addChild:backgroundMid];
            [mainScene addChild:backgroundNodeEquip];
            
            [backgroundMid runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:backgroundMidTextures timePerFrame: 0.08 resize:NO restore:YES]]withKey:@"midRunning"];
            
            [backgroundBottom runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:backgroundBottomTextures timePerFrame: 0.1 resize:NO restore:YES]]withKey:@"bottomRunning"];
            
            [scanLine runAction:runScanSeq completion:^{
                scanLine.hidden = TRUE;
            }];
            [mainScene addChild:scanLine];
            [backgroundBottom addChild:emitterEffect];
            [backgroundNodeEquip addChild:generalTab];
            [backgroundNodeEquip addChild:weaponsTab];
            [backgroundNodeEquip addChild:defenseTab];
            [backgroundNodeEquip addChild:systemTab];
            break;
        case 1:
            [GameScene tab1POS:1];
            [GameScene tab2POS:0];
            [GameScene tab3POS:0];
            [GameScene tab4POS:0];
            if ([GameScene getAudioPOS] == 0) {
                [mainScene runAction:clickedSound];
            }
            [self deactivateGeneralTab];
            [self deactivateWeaponTab];
            [self deactivateDefenseTab];
            [self deactivateSystemTab];
        
            
            break;
        default:
            break;
    }
}

+(void)closeEquipmentView{
    isPresented = 0;
    [backgroundBottom removeFromParent];
    [backgroundMid removeFromParent];
    [backgroundNodeEquip removeFromParent];
    [scanLine removeFromParent];
    [TopMenu removeTopMenu];
    [self deactivateGeneralTab];
    [self deactivateWeaponTab];
    [self deactivateDefenseTab];
    [self deactivateSystemTab];
}


//Equipment view Tab Actions
+(void)activateGeneralTab{
    scanLine.hidden = FALSE;
    generalTab.texture = [SKTexture textureWithImageNamed:@"generalActive"];
    [scanLine runAction:runScanSeq completion:^{
        scanLine.hidden = TRUE;
    }];
    [GeneralTab presentGeneralTab:mainScene viewBackground:backgroundNodeEquip ship:theShip];
}

+(void)deactivateGeneralTab{

    generalTab.texture = [SKTexture textureWithImageNamed:@"generalInactive"];
    [GeneralTab removeGeneralTab];
}

+(void)activateWeaponTab{
    scanLine.hidden = FALSE;
    weaponsTab.texture = [SKTexture textureWithImageNamed:@"weaponsActive"];
    [scanLine runAction:runScanSeq completion:^{
        scanLine.hidden = TRUE;
    }];
    [WeaponTab presentWeaponsTab:mainScene viewBackground:backgroundNodeEquip allWeapons:weaponsAll weaponsFixed:weaponsArrayFixed weaponsGimbal:weaponsArrayGimbal theShip:theShip];
    
}

+(void)deactivateWeaponTab{

    weaponsTab.texture = [SKTexture textureWithImageNamed:@"weaponsInactive"];
    [WeaponTab removeWeaponsTab];
}
+(void)activateDefenseTab{
    scanLine.hidden = FALSE;
    defenseTab.texture = [SKTexture textureWithImageNamed:@"defenseActive"];
    [scanLine runAction:runScanSeq completion:^{
        scanLine.hidden = TRUE;
    }];
    [DefenseTab presentDefenseTab:mainScene viewBackground:backgroundNodeEquip defenseEquip:defenseArray theShip:theShip];
    
}
+(void)deactivateDefenseTab{

    defenseTab.texture = [SKTexture textureWithImageNamed:@"defenseInactive"];
    [DefenseTab removeDefenseTab];
}

+(void)activateSystemTab{
    scanLine.hidden = FALSE;
    systemTab.texture = [SKTexture textureWithImageNamed:@"systemsActive"];
    [scanLine runAction:runScanSeq completion:^{
        scanLine.hidden = TRUE;
    }];
    [SystemTab presentSystemTab:mainScene viewBackground:backgroundNodeEquip systemEquip:systemArray];
}
+(void)deactivateSystemTab{

    systemTab.texture = [SKTexture textureWithImageNamed:@"systemsInactive"];
    [SystemTab removeSystemTab];
}

+(void)activeShipSelector{
    [TopMenu presentSelectShipMenu:mainScene];
}
+(void)deactivateShipSelector{
    [TopMenu removeSelectShipMenu];
}
+(void)hideView:(BOOL)isHidden{
    backgroundBottom.hidden = isHidden;
    backgroundMid.hidden = isHidden;
    backgroundNodeEquip.hidden = isHidden;
    scanLine.hidden = isHidden;
}

@end
