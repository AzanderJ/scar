//
//  TopMenu.m
//  SCAR
//
//  Created by Alexzander Jaggi on 3/31/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import "TopMenu.h"

@implementation TopMenu

SKSpriteNode * topBar;
SKSpriteNode * shipButton;
SKSpriteNode * layoutButton;
UITableView * shipTable;
SKSpriteNode * tableBackground;
SKScene * primaryScene;
NSArray * shipList;
bool firstLaunch;
NSUserDefaults * theD;



+(TopMenu *)initTopMenu{
    static TopMenu * theTopMenu;
    
    @synchronized (self) {
        if (!theTopMenu) {
            theTopMenu = [[self alloc]init];
        }
    }
    return theTopMenu;
}

+(void)presentTopMenu:(SKScene *)theScene viewBackground:(SKSpriteNode *)backgroundNode{
    
    //reload menu if first launch
    shipList = [FirebaseData getShipList];
    theD = [NSUserDefaults standardUserDefaults];
    if (![theD objectForKey:@"first"]) {
        shipList = [FirebaseData getShipList];
        NSDictionary * hasStarted = @{@"firstRun": @0};
        [theD rm_setCustomObject:hasStarted forKey:@"first"];
        firstLaunch = YES;
        [shipTable reloadData];
    }
    
    
    topBar = [SKSpriteNode spriteNodeWithImageNamed:@"topDock"];
    topBar.size = CGSizeMake(backgroundNode.frame.size.width/1.2, topBar.frame.size.height/2.5);
    topBar.position = CGPointMake(theScene.size.width - backgroundNode.size.width/1.85, theScene.size.height + topBar.size.height/9);
    topBar.zPosition = 6;
    topBar.alpha = 0.8;
    
    shipButton = [SKSpriteNode spriteNodeWithImageNamed:@"shipsLabel2"];
    shipButton.size = CGSizeMake(topBar.frame.size.width/6, topBar.size.height);
    shipButton.position = CGPointMake(-topBar.frame.origin.x-shipButton.size.width/1.5, -shipButton.size.height/2.3);
    shipButton.zPosition = 7;
    shipButton.name = @"shipButton";
    shipButton.alpha = 0.9;
    
    
    //The ship selector Tableview
    shipTable = [[UITableView alloc]initWithFrame:CGRectMake(topBar.frame.origin.x, shipButton.frame.size.height/2.8, backgroundNode.size.width/3, backgroundNode.size.height)];
    shipTable.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"shipMenuBG"]];
    shipTable.backgroundView.alpha = 0.9;
    shipTable.backgroundColor = [UIColor clearColor];
    shipTable.separatorColor = [UIColor colorWithRed:0 green:0.659 blue:0.91 alpha:1]; //#00a8e8
    [shipTable setSeparatorInset:UIEdgeInsetsMake(12, 12, 12, 12)];
    
    //-----------///-------------//
    
    [[TopMenu initTopMenu]setDelegate];
    [theScene addChild:topBar];
    [topBar addChild:shipButton];
}
+(void)removeTopMenu{
    [topBar removeFromParent];
    [shipTable removeFromSuperview];
}

+(void)presentSelectShipMenu:(SKScene *)theScene {
    
    [theScene.view addSubview:shipTable];
    if (firstLaunch) {
        
        [shipTable reloadData];
    }
}

+(void)removeSelectShipMenu{
    [shipTable removeFromSuperview];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [shipList count];
}

-(void)setDelegate{
    shipTable.dataSource = self;
    shipTable.delegate = self;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    Ships * theShips = [shipList objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    
    //Temp Cell
    cell.textLabel.text = theShips.shipModel;
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont fontWithName:@"OratorStd" size:18];
    cell.textLabel.alpha = 0.8;
    cell.detailTextLabel.alpha = 0.8;
    cell.selectedBackgroundView = [UIView new];
    cell.selectedBackgroundView.layer.cornerRadius = 10;
    cell.selectedBackgroundView.alpha = 0.5;
    cell.selectedBackgroundView.layer.masksToBounds = YES;
    cell.selectedBackgroundView.backgroundColor = [UIColor colorWithRed:0 green:0.494 blue:0.655 alpha:0.4]; //#007ea7
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [EquipmentView presentDataViews:[shipList objectAtIndex:indexPath.row]];
    [EquipmentView activateGeneralTab];
    [shipTable removeFromSuperview];
}
+(SKSpriteNode *)getTopBar{
    return topBar;
}
+(void)hideView:(BOOL)isHidden{
    topBar.hidden = isHidden;
}
@end
