//
//  WeaponTab.m
//  SCAR
//
//  Created by Alexzander Jaggi on 3/24/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import "WeaponTab.h"

UITableView * availableWeaponsTable;
UITableView * shipWeaponSpots;
NSArray * aWeaponsList;
NSArray * fixedWepArray;
NSArray * gimbalWepArray;
Ships * selectedShip;
UILabel * shipNameW;
UITextView * descTextW;
SKSpriteNode * theTopBarW;
SKSpriteNode * saveButtonDW;
SKSpriteNode * loadButtonDW;
SKSpriteNode * saveButtonAW;
SKSpriteNode * loadButtonAW;
SKSpriteNode * deleteButtonDW;
SKSpriteNode * deleteButtonAW;
SKSpriteNode * simButtonDW;
SKSpriteNode * simButtonAW;
SKScene * theMainSceneWT;
int loadActiveW = 0;
int saveActiveW = 0;
int deleteActiveW = 0;
int simActiveW = 0;



@implementation WeaponTab

+(WeaponTab *)initWeaponsTab{
    static WeaponTab * theWeaponsTab;

    @synchronized (self) {
        if (!theWeaponsTab) {
            theWeaponsTab = [[self alloc]init];
        }
    }
    return theWeaponsTab;
}

+(void)presentWeaponsTab:(SKScene *)theScene viewBackground:(SKSpriteNode *)backgroundNode allWeapons:(NSArray *)allWeapons weaponsFixed:(NSArray *)theFixedWeapons weaponsGimbal:(NSArray *)theGimbalWeapons theShip:(Ships *)theShip{
    
    aWeaponsList = @[];
    fixedWepArray = theFixedWeapons;
    gimbalWepArray = theGimbalWeapons;
    theMainSceneWT = theScene;
    
    theTopBarW = [TopMenu getTopBar];
    
    selectedShip = theShip;
    
    shipNameW = [[UILabel alloc]initWithFrame:CGRectMake(backgroundNode.size.width/3, backgroundNode.size.height/4.5, backgroundNode.size.width/1.5, backgroundNode.size.height/10)];
    shipNameW.text = [NSString stringWithFormat:@"%@: %@",theShip.shipBrand, theShip.shipModel];
    shipNameW.font = [UIFont fontWithName:@"OratorStd" size:42];
    shipNameW.adjustsFontSizeToFitWidth = TRUE;
    shipNameW.textAlignment = NSTextAlignmentCenter;
    shipNameW.textColor = [UIColor whiteColor];
    shipNameW.alpha = 0.8;
    
    descTextW = [[UITextView alloc]initWithFrame:CGRectMake(backgroundNode.size.width/5, backgroundNode.size.height/3, backgroundNode.size.width/2.3, backgroundNode.size.height/20)];
    descTextW.backgroundColor = [UIColor clearColor];
    descTextW.font = [UIFont fontWithName:@"OratorStd" size:18];
    descTextW.textColor = [UIColor whiteColor];
    descTextW.textAlignment = NSTextAlignmentCenter;
    descTextW.editable = NO;
    descTextW.layer.borderWidth = 1;
    descTextW.layer.borderColor = [[UIColor colorWithRed:0 green:0.659 blue:0.91 alpha:0.4]CGColor]; //#00a8e8
    descTextW.layer.cornerRadius = 10;
    descTextW.scrollEnabled = FALSE;
    descTextW.alpha = 0.8;
    
    // available weapons tableview
    
    availableWeaponsTable = [[UITableView alloc]initWithFrame:CGRectMake(backgroundNode.size.width/1.5, backgroundNode.size.height/3, backgroundNode.size.width/2.2, backgroundNode.size.height/1.3)];
    availableWeaponsTable.backgroundColor = [UIColor clearColor];
    availableWeaponsTable.separatorColor = [UIColor colorWithRed:0 green:0.659 blue:0.91 alpha:1]; //#00a8e8
    availableWeaponsTable.tag = 2;
    availableWeaponsTable.tableFooterView = [UIView new];
    [availableWeaponsTable setSeparatorInset:UIEdgeInsetsMake(0, 55, 0, 0)];
    
    // ship hard points
    
    shipWeaponSpots = [[UITableView alloc]initWithFrame:CGRectMake(backgroundNode.size.width/5, backgroundNode.size.height/2.5, backgroundNode.size.width/2.2, backgroundNode.size.height/1.425)];
    shipWeaponSpots.backgroundColor = [UIColor clearColor];
    shipWeaponSpots.separatorColor = [UIColor colorWithRed:0 green:0.659 blue:0.91 alpha:1]; //#00a8e8
    shipWeaponSpots.tag = 0;
    shipWeaponSpots.tableFooterView = [UIView new];
    [shipWeaponSpots setSeparatorInset:UIEdgeInsetsMake(0, 55, 0, 0)];
    
    saveButtonDW = [SKSpriteNode spriteNodeWithImageNamed:@"saveDark"];
    saveButtonDW.size = CGSizeMake(theTopBarW.frame.size.width/8, theTopBarW.size.height/2.5);
    saveButtonDW.position = CGPointMake(theTopBarW.size.width/10-saveButtonDW.size.width, -saveButtonDW.size.height/1.4);
    saveButtonDW.zPosition = 7;
    saveButtonDW.alpha = 0.75;
    
    loadButtonDW = [SKSpriteNode spriteNodeWithImageNamed:@"loadDark"];
    loadButtonDW.size = CGSizeMake(theTopBarW.frame.size.width/8, theTopBarW.size.height/2.5);
    loadButtonDW.position = CGPointMake(theTopBarW.size.width/4.5-loadButtonDW.size.width, -loadButtonDW.size.height/1.4);
    loadButtonDW.zPosition =7;
    loadButtonDW.alpha = 0.75;
    
    saveButtonAW = [SKSpriteNode spriteNodeWithImageNamed:@"saveLight"];
    saveButtonAW.size = CGSizeMake(theTopBarW.frame.size.width/8, theTopBarW.size.height/2.5);
    saveButtonAW.position = CGPointMake(theTopBarW.size.width/10-saveButtonDW.size.width, -saveButtonDW.size.height/1.4);
    saveButtonAW.name = @"save";
    saveButtonAW.zPosition = 7;
    saveButtonAW.alpha = 0.75;
    
    loadButtonAW = [SKSpriteNode spriteNodeWithImageNamed:@"loadLight"];
    loadButtonAW.size = CGSizeMake(theTopBarW.frame.size.width/8, theTopBarW.size.height/2.5);
    loadButtonAW.position = CGPointMake(theTopBarW.size.width/4.5-loadButtonDW.size.width, -loadButtonDW.size.height/1.4);
    loadButtonAW.zPosition =7;
    loadButtonAW.name = @"load";
    loadButtonAW.alpha = 0.75;
    
    deleteButtonDW = [SKSpriteNode spriteNodeWithImageNamed:@"deleteDark"];
    deleteButtonDW.size = CGSizeMake(theTopBarW.frame.size.width/8, theTopBarW.size.height/2.5);
    deleteButtonDW.position = CGPointMake(theTopBarW.size.width/2.91-deleteButtonDW.size.width, -deleteButtonDW.size.height/1.4);
    deleteButtonDW.zPosition =7;
    deleteButtonDW.alpha = 0.75;
    
    deleteButtonAW = [SKSpriteNode spriteNodeWithImageNamed:@"deleteLight"];
    deleteButtonAW.size = CGSizeMake(theTopBarW.frame.size.width/8, theTopBarW.size.height/2.5);
    deleteButtonAW.position = CGPointMake(theTopBarW.size.width/2.91-deleteButtonDW.size.width, -deleteButtonDW.size.height/1.4);
    deleteButtonAW.zPosition =7;
    deleteButtonAW.name = @"delete";
    deleteButtonAW.alpha = 0.75;
    
    simButtonDW = [SKSpriteNode spriteNodeWithImageNamed:@"testDark"];
    simButtonDW.size = CGSizeMake(theTopBarW.frame.size.width/8, theTopBarW.size.height/2.5);
    simButtonDW.position = CGPointMake(theTopBarW.size.width/1.8-simButtonDW.size.width, -simButtonDW.size.height/1.4);
    simButtonDW.zPosition =7;
    simButtonDW.alpha = 0.75;
    
    simButtonAW = [SKSpriteNode spriteNodeWithImageNamed:@"testLight"];
    simButtonAW.size = CGSizeMake(theTopBarW.frame.size.width/8, theTopBarW.size.height/2.5);
    simButtonAW.position = CGPointMake(theTopBarW.size.width/1.8-simButtonDW.size.width, -simButtonDW.size.height/1.4);
    simButtonAW.zPosition =7;
    simButtonAW.name = @"sim";
    simButtonAW.alpha = 0.75;
    
    [theTopBarW addChild:saveButtonDW];
    [theTopBarW addChild:loadButtonDW];
    [theTopBarW addChild:deleteButtonDW];
    [theTopBarW addChild:simButtonDW];

    
    NSUserDefaults * shipSavedW = [NSUserDefaults standardUserDefaults];
    NSDictionary * weaponsSaved = [shipSavedW rm_customObjectForKey:[NSString stringWithFormat:@"%@Weapons",theShip.shipModel]];
    if (weaponsSaved != nil) {
        [loadButtonDW removeFromParent];
        [theTopBarW addChild:loadButtonAW];
        loadActiveW = 1;
    }
    //-----------///-------------//
    
    [theScene.view addSubview:availableWeaponsTable];
    [theScene.view addSubview:shipWeaponSpots];
    [theScene.view addSubview:shipNameW];
    [theScene.view addSubview:descTextW];
    [[WeaponTab initWeaponsTab]loadDelegates];
}

+(void)removeWeaponsTab{
    [availableWeaponsTable removeFromSuperview];
    [shipWeaponSpots removeFromSuperview];
    [shipNameW removeFromSuperview];
    [descTextW removeFromSuperview];
    if (loadActiveW == 1) {
        [loadButtonAW removeFromParent];
        loadActiveW = 0;
    }else{
        [loadButtonDW removeFromParent];
        loadActiveW = 0;
    }
    if (saveActiveW == 1) {
        [saveButtonAW removeFromParent];
        saveActiveW = 0;
    }else{
        [saveButtonDW removeFromParent];
        saveActiveW = 0;
    }
    if (deleteActiveW == 1) {
        [deleteButtonAW removeFromParent];
        deleteActiveW = 0;
    }else{
        [deleteButtonDW removeFromParent];
        deleteActiveW = 0;
    }
    if (simActiveW == 1) {
        [simButtonAW removeFromParent];
        simActiveW = 0;
    }else{
        [simButtonDW removeFromParent];
        simActiveW = 0;
    }
}

-(void)loadDelegates{
    ShipEquipmentDelegate * equipDelegate = [ShipEquipmentDelegate initShipMountDelegateWithShip:selectedShip theWeapons:aWeaponsList theFixedWep:fixedWepArray theGimbalArray:gimbalWepArray theAvalibleDefense:nil theScene:theMainSceneWT];
    availableWeaponsTable.delegate = equipDelegate;
    availableWeaponsTable.dataSource = equipDelegate;
    shipWeaponSpots.delegate = equipDelegate;
    shipWeaponSpots.dataSource = equipDelegate;
    
}
+(void)reloadAvaliWeaponTable{
    [availableWeaponsTable reloadData];
}
+(void)reloadShipMountsTable:(NSIndexPath *)unselectPath selectPath:(NSIndexPath *)selectPath{
    [shipWeaponSpots reloadData];
    [availableWeaponsTable deselectRowAtIndexPath:unselectPath animated:YES];
    [shipWeaponSpots selectRowAtIndexPath:selectPath animated:NO scrollPosition:NO];
    
}
+(void)activateSaveButton{
    if (saveActiveW == 0) {
        [saveButtonDW removeFromParent];
        [theTopBarW addChild:saveButtonAW];
        saveActiveW = 1;
    }
    
}
+(void)deactivateSaveButton{
    if (saveActiveW == 1) {
        [saveButtonAW removeFromParent];
        [theTopBarW addChild:saveButtonDW];
        saveActiveW = 0;
    }
    
}
+(void)activateLoadButton{
    if (loadActiveW == 0) {
        [loadButtonDW removeFromParent];
        [theTopBarW addChild:loadButtonAW];
        loadActiveW = 1;
    }
    
}
+(void)deactivateLoadButton{
    if (loadActiveW == 1) {
        [loadButtonAW removeFromParent];
        [theTopBarW addChild:loadButtonDW];
        loadActiveW = 0;
    }
    
}
+(void)activateDeleteButton{
    if (deleteActiveW == 0) {
        [deleteButtonDW removeFromParent];
        [theTopBarW addChild:deleteButtonAW];
        deleteActiveW = 1;
    }
    
}
+(void)deactivateDeleteButton{
    if (deleteActiveW == 1) {
        [deleteButtonAW removeFromParent];
        [theTopBarW addChild:deleteButtonDW];
        deleteActiveW = 0;
    }
    
}
+(void)activateSimButton{
    if (simActiveW == 0) {
        [simButtonDW removeFromParent];
        [theTopBarW addChild:simButtonAW];
        simActiveW = 1;
    }
    
}
+(void)deactivateSimButton{
    if (simActiveW == 1) {
        [simButtonAW removeFromParent];
        [theTopBarW addChild:simButtonDW];
        simActiveW = 0;
    }
    
}
+(void)hideView:(BOOL)isHidden{
    availableWeaponsTable.hidden = isHidden;
    shipWeaponSpots.hidden = isHidden;
    shipNameW.hidden = isHidden;
    descTextW.hidden = isHidden;
}
+(void)setDescText{
    descTextW.text = [NSString stringWithFormat:@"Damage Per Second: %@",[ShipEquipmentDelegate getDPS]];
}
@end
