//
//  SystemTab.h
//  SCAR
//
//  Created by Alexzander Jaggi on 4/9/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "GameScene.h"
#import "SystemEquipment.h"

@interface SystemTab : NSObject <UITableViewDelegate, UITableViewDataSource>

+(void)presentSystemTab:(SKScene *)theScene viewBackground:(SKSpriteNode *)backgroundNode systemEquip:(NSDictionary *)theEquip;
+(void)removeSystemTab;

@end
