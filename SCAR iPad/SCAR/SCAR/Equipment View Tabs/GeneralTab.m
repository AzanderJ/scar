//
//  GeneralTab.m
//  SCAR
//
//  Created by Alexzander Jaggi on 3/24/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import "GeneralTab.h"

@implementation GeneralTab

UITextView * shipDesc;
UIImageView * imgView;
UILabel * shipName;
UIView * lowerSection;

+(void)presentGeneralTab:(SKScene *)theScene viewBackground:(SKSpriteNode *)backgroundNode ship:(Ships *)theShip{
    
    //General page Title
    shipName = [[UILabel alloc]initWithFrame:CGRectMake(backgroundNode.size.width/3, backgroundNode.size.height/4.5, backgroundNode.size.width/1.5, backgroundNode.size.height/10)];
    shipName.text = [NSString stringWithFormat:@"%@: %@",theShip.shipBrand, theShip.shipModel];
    shipName.font = [UIFont fontWithName:@"OratorStd" size:42];
    shipName.adjustsFontSizeToFitWidth = TRUE;
    shipName.textAlignment = NSTextAlignmentCenter;
    shipName.textColor = [UIColor whiteColor];
    shipName.alpha = 0.8;
    
    //Ship Image
    
    imgView = [[UIImageView alloc]initWithFrame:CGRectMake(backgroundNode.size.width/1.6, backgroundNode.size.height/2.2, 80, 80)];
    imgView.image = [UIImage imageNamed:theShip.shipIMG];
    imgView.contentMode = UIViewContentModeScaleAspectFill;
    imgView.alpha = 0.85;

    
    //Ship Description
    
    shipDesc = [[UITextView alloc]initWithFrame:CGRectMake(backgroundNode.size.width/3, backgroundNode.size.height/1.4, backgroundNode.size.width/1.5, backgroundNode.size.height/4)];
    shipDesc.text = theShip.shipDescription;
    shipDesc.font = [UIFont fontWithName:@"OratorStd" size:18];
    shipDesc.textAlignment = NSTextAlignmentCenter;
    shipDesc.backgroundColor = [UIColor clearColor];
    shipDesc.textColor = [UIColor whiteColor];
    shipDesc.editable = NO;
    shipDesc.alpha = 0.8;

    lowerSection = [[UIView alloc]initWithFrame:CGRectMake(backgroundNode.size.width/3, backgroundNode.size.height, backgroundNode.size.width/1.5, backgroundNode.size.height/8)];
    lowerSection.backgroundColor = [UIColor clearColor];
    
    //lower section icons
    UIImageView * fixedICO = [[UIImageView alloc]initWithFrame:CGRectMake(25, 5, 50, 50)];
    fixedICO.image = [UIImage imageNamed:@"fixedICO2"];
    fixedICO.contentMode = UIViewContentModeScaleAspectFill;
    fixedICO.alpha = 0.6;
    
    UIImageView * gimbalICO = [[UIImageView alloc]initWithFrame:CGRectMake(lowerSection.frame.size.width/2 - 25, 5, 50, 50)];
    gimbalICO.image = [UIImage imageNamed:@"gimbalICO2"];
    gimbalICO.contentMode = UIViewContentModeScaleAspectFill;
    gimbalICO.alpha = 0.6;
    
    UIImageView * shieldICO = [[UIImageView alloc]initWithFrame:CGRectMake(lowerSection.frame.size.width - 75, 5, 50, 50)];
    shieldICO.image = [UIImage imageNamed:@"shieldICO2"];
    shieldICO.contentMode = UIViewContentModeScaleAspectFill;
    shieldICO.alpha = 0.6;
    
    //lower stats
    UILabel * fixedLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, lowerSection.frame.size.height-10, 100, 20)];
    fixedLabel.backgroundColor = [UIColor clearColor];
    fixedLabel.font = [UIFont fontWithName:@"OratorStd" size:18];
    fixedLabel.textColor = [UIColor whiteColor];
    fixedLabel.text = [NSString stringWithFormat:@"%@X Size %@",theShip.shipFixedAmount, theShip.shipFixedSize];
    fixedLabel.alpha = 0.8;
    
    UILabel * gimbalLabel = [[UILabel alloc]initWithFrame:CGRectMake(lowerSection.frame.size.width/2 - 50, lowerSection.frame.size.height-10, 100, 20)];
    gimbalLabel.backgroundColor = [UIColor clearColor];
    gimbalLabel.font = [UIFont fontWithName:@"OratorStd" size:18];
    gimbalLabel.textColor = [UIColor whiteColor];
    gimbalLabel.text = [NSString stringWithFormat:@"%@X Size %@",theShip.shipGimbaledAmount, theShip.shipGimbalSize];
    gimbalLabel.alpha = 0.8;
    
    UILabel * shieldLabel = [[UILabel alloc]initWithFrame:CGRectMake(lowerSection.frame.size.width-100, lowerSection.frame.size.height-10, 100, 20)];
    shieldLabel.backgroundColor = [UIColor clearColor];
    shieldLabel.font = [UIFont fontWithName:@"OratorStd" size:18];
    shieldLabel.textColor = [UIColor whiteColor];
    shieldLabel.text = [NSString stringWithFormat:@"Size %@",theShip.shipShield];
    shieldLabel.textAlignment = NSTextAlignmentCenter;
    shieldLabel.alpha = 0.8;
    
    //-----------///-------------//

    [theScene.view addSubview:shipName];
    [theScene.view addSubview:shipDesc];
    [theScene.view addSubview:imgView];
    [theScene.view addSubview:lowerSection];
    [lowerSection addSubview:fixedICO];
    [lowerSection addSubview:gimbalICO];
    [lowerSection addSubview:shieldICO];
    [lowerSection addSubview:fixedLabel];
    [lowerSection addSubview:gimbalLabel];
    [lowerSection addSubview:shieldLabel];
}

+(void)removeGeneralTab{
    [shipName removeFromSuperview];
    [shipDesc removeFromSuperview];
    [imgView removeFromSuperview];
    [lowerSection removeFromSuperview];
}

@end
