//
//  DefenseTab.h
//  SCAR
//
//  Created by Alexzander Jaggi on 4/8/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

#import "DefenseEquipment.h"
#import "TopMenu.h"
#import "DefenseDelegate.h"

@interface DefenseTab : NSObject

+(void)presentDefenseTab:(SKScene *)theScene viewBackground:(SKSpriteNode *)backgroundNode defenseEquip:(NSArray *)theEquip theShip:(Ships *)theShip;
+(void)removeDefenseTab;
+(void)reloadDefenseShipMounts:(NSIndexPath *)unselectPath selectPath:(NSIndexPath *)selectPath;
+(void)activateSaveButton;
+(void)deactivateSaveButton;
+(void)activateLoadButton;
+(void)deactivateLoadButton;
+(void)activateDeleteButton;
+(void)deactivateDeleteButton;
+(void)activateSimButton;
+(void)deactivateSimButton;
+(void)hideView:(BOOL)isHidden;
@end
