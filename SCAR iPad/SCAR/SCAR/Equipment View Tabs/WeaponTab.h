//
//  WeaponTab.h
//  SCAR
//
//  Created by Alexzander Jaggi on 3/24/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Weapons.h"
#import "Ships.h"
#import "ShipEquipmentDelegate.h"
#import "TopMenu.h"

@interface WeaponTab : NSObject

+(void)presentWeaponsTab:(SKScene *)theScene viewBackground:(SKSpriteNode *)backgroundNode allWeapons:(NSArray *)allWeapons weaponsFixed:(NSArray *)theFixedWeapons weaponsGimbal:(NSArray *)theGimbalWeapons theShip:(Ships *)theShip;
+(void)removeWeaponsTab;
+(void)reloadAvaliWeaponTable;
+(void)reloadShipMountsTable:(NSIndexPath *)unselectPath selectPath:(NSIndexPath *)selectPath;
+(void)activateSaveButton;
+(void)deactivateSaveButton;
+(void)activateLoadButton;
+(void)deactivateLoadButton;
+(void)activateDeleteButton;
+(void)deactivateDeleteButton;
+(void)activateSimButton;
+(void)deactivateSimButton;
+(void)hideView:(BOOL)isHidden;
+(void)setDescText;

@end
