//
//  Settings.m
//  SCAR
//
//  Created by Alexzander Jaggi on 4/26/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import "Settings.h"

SKSpriteNode * backG;
SKSpriteNode * backgroundMidS;
SKSpriteNode * backgroundBottomS;
SKSpriteNode * infoButton;
SKSpriteNode * creditsBG;
SKScene * theSceneSet;
SKSpriteNode * doneButtonCredits;
SKSpriteNode * audioButton;
NSArray * backgroundBottomTexturesS;
NSArray * backgroundMidTexturesS;
SKEmitterNode * emitterEffectS;

@implementation Settings
+(void)initSettings:(SKScene *)theScene{
    theSceneSet = theScene;
    backG = [SKSpriteNode spriteNodeWithImageNamed:@"topDock"];
    backG.size = CGSizeMake(200, 60);
    backG.position = CGPointMake(theScene.size.width-backG.size.width/2, theScene.size.height - backG.size.height/1.65);
    backG.zPosition = 18;
    backG.alpha = 0.8;
    
    infoButton = [SKSpriteNode spriteNodeWithImageNamed:@"information"];
    infoButton.size = CGSizeMake(40, 40);
    infoButton.position = CGPointMake(backG.size.width/10-30, backG.size.height/2 - infoButton.size.height/1.4);
    infoButton.zPosition = 7;
    infoButton.name = @"infoButton";
    
    creditsBG = [SKSpriteNode spriteNodeWithImageNamed:@"backgroundTop"];
    creditsBG.size = CGSizeMake(theScene.size.width, theScene.size.height);
    creditsBG.position = CGPointMake(theScene.size.width/2, theScene.size.height/2);
    creditsBG.zPosition = 20;
    creditsBG.alpha = 0.75;
    
    NSMutableArray * midFrames = [[NSMutableArray alloc]init];
    SKTextureAtlas * midTextureAtlas = [SKTextureAtlas atlasNamed:@"mid"];
    long midTextureCount = midTextureAtlas.textureNames.count;
    for (int i=1; i <= midTextureCount; i++) {
        NSString * midTextureName = [NSString stringWithFormat:@"midLayer%d",i];
        SKTexture * midTempTexture = [midTextureAtlas textureNamed:midTextureName];
        [midFrames addObject:midTempTexture];
    }
    backgroundMidTexturesS = [[NSArray alloc]initWithArray:midFrames];
    
    backgroundMidS = [SKSpriteNode spriteNodeWithImageNamed:@"midLayer1"];
    backgroundMidS.size = CGSizeMake(theScene.size.width, theScene.size.height);
    backgroundMidS.position = CGPointMake(theScene.size.width/2, theScene.size.height/2);
    backgroundMidS.zPosition = 19;
    backgroundMidS.alpha = 0.3;
    
    NSMutableArray * bottomFrames = [[NSMutableArray alloc]init];
    SKTextureAtlas * bottomTextureAtlas = [SKTextureAtlas atlasNamed:@"bottom"];
    long bottomTextureCount = bottomTextureAtlas.textureNames.count;
    for (int j=1; j <= bottomTextureCount; j++) {
        NSString * bottomTextureName = [NSString stringWithFormat:@"bottomLayer%d",j];
        SKTexture * bottomTempTexture = [bottomTextureAtlas textureNamed:bottomTextureName];
        [bottomFrames addObject:bottomTempTexture];
    }
    backgroundBottomTexturesS = [[NSArray alloc]initWithArray:bottomFrames];
    
    backgroundBottomS = [SKSpriteNode spriteNodeWithTexture:bottomFrames[0]];
    backgroundBottomS.size = CGSizeMake(theScene.size.width, theScene.size.height);
    backgroundBottomS.position = CGPointMake(theScene.size.width/2, theScene.size.height/2);
    backgroundBottomS.zPosition = 18;
    backgroundBottomS.alpha = 0.5;
    
    emitterEffectS = [SKEmitterNode nodeWithFileNamed:@"ScanParticals"];
    emitterEffectS.particlePositionRange = CGVectorMake(theScene.frame.size.width, theScene.frame.size.height);
    emitterEffectS.zPosition = 0;

    doneButtonCredits = [SKSpriteNode spriteNodeWithImageNamed:@"doneButton"];
    doneButtonCredits.yScale = 0.25;
    doneButtonCredits.xScale = 0.25;
    doneButtonCredits.position = CGPointMake(creditsBG.size.width/1.9 - doneButtonCredits.size.width,-creditsBG.size.height/2.25);
    doneButtonCredits.name = @"doneCredits";
    doneButtonCredits.zPosition = 21;
    
    audioButton = [SKSpriteNode spriteNodeWithImageNamed:@"audioON"];
    audioButton.xScale = 0.055;
    audioButton.yScale = 0.055;
    audioButton.position = CGPointMake(-backG.size.width/3, backG.size.height/2 - infoButton.size.height/1.4);
    audioButton.zPosition = 7;
    audioButton.name = @"audio";
    audioButton.alpha = 0.85;
    if ([GameScene getAudioPOS] == 1) {
        audioButton.texture = [SKTexture textureWithImage:[UIImage imageNamed:@"audioOFF"]];
    }
    [backgroundMidS runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:backgroundMidTexturesS timePerFrame: 0.08 resize:NO restore:YES]]withKey:@"midRunning"];
    
    [backgroundBottomS runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:backgroundBottomTexturesS timePerFrame: 0.1 resize:NO restore:YES]]withKey:@"bottomRunning"];
    
    [theScene addChild:backG];
    [backG addChild:infoButton];
    [backG addChild:audioButton];
    
}

+(void)removeSettings{
    [backG removeFromParent];
}
+(void)addCreditsBG{
    [theSceneSet addChild:backgroundBottomS];
    [theSceneSet addChild:backgroundMidS];
    [theSceneSet addChild:creditsBG];
    [creditsBG addChild:emitterEffectS];
    [creditsBG addChild:doneButtonCredits];
}
+(void)removeCreditsBG{
    [backgroundMidS removeFromParent];
    [backgroundBottomS removeFromParent];
    [creditsBG removeFromParent];
    [doneButtonCredits removeFromParent];
}
+(SKSpriteNode *)getCreditsBG{
    return creditsBG;
}
+(void)setAudioPOS:(int)pos{
    
    switch (pos) {
        case 0:
            audioButton.texture = [SKTexture textureWithImage:[UIImage imageNamed:@"audioON"]];
            break;
        case 1:
            audioButton.texture = [SKTexture textureWithImage:[UIImage imageNamed:@"audioOFF"]];
            break;
            
        default:
            break;
    }
}

@end
