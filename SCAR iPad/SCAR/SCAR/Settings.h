//
//  Settings.h
//  SCAR
//
//  Created by Alexzander Jaggi on 4/26/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "GameScene.h"

@interface Settings : NSObject

+(void)initSettings:(SKScene *)theScene;
+(void)removeSettings;
+(void)addCreditsBG;
+(void)removeCreditsBG;
+(SKSpriteNode *)getCreditsBG;
+(void)setAudioPOS:(int)pos;
@end
