//
//  GameViewController.h
//  SCAR
//

//  Copyright (c) 2016 Alexzander Jaggi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import "TopMenu.h"

@interface GameViewController : UIViewController

@end
