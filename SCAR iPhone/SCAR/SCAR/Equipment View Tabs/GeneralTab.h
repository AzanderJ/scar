//
//  GeneralTab.h
//  SCAR
//
//  Created by Alexzander Jaggi on 3/24/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import <UIKit/UIKit.h>
#import "Ships.h"

@interface GeneralTab : NSObject

+(void)presentGeneralTab:(SKScene *)theScene viewBackground:(SKSpriteNode *)backgroundNode ship:(Ships *)theShip;
+(void)removeGeneralTab;

@end
