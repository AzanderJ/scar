//
//  DefenseTab.m
//  SCAR
//
//  Created by Alexzander Jaggi on 4/8/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import "DefenseTab.h"

UITableView * availableDefenseEquip;
UITableView * shipDefenseSpots;
NSArray * aDefenseEquip;
Ships * defship;
UILabel * shipNameD;
SKSpriteNode * theTopBarD;
SKSpriteNode * saveButtonDD;
SKSpriteNode * saveButtonAD;
SKSpriteNode * loadButtonDD;
SKSpriteNode * loadButtonAD;
SKSpriteNode * deleteButtonDD;
SKSpriteNode * deleteButtonAD;
SKSpriteNode * simButtonDD;
SKSpriteNode * simButtonAD;
SKScene * theSceneD;

int loadButtonActive = 0;
int saveButtonActive = 0;
int deleteButtonActive = 0;
int simButtonActive = 0;

@implementation DefenseTab

+(DefenseTab *)initDefenseTab{
    static DefenseTab * theDefenseTab;

    @synchronized (self) {
        if (!theDefenseTab) {
            theDefenseTab = [[self alloc]init];
        }
    }
    return theDefenseTab;
}

+(void)presentDefenseTab:(SKScene *)theScene viewBackground:(SKSpriteNode *)backgroundNode defenseEquip:(NSArray *)theEquip theShip:(Ships *)theShip{
    
    aDefenseEquip = theEquip;
    defship = theShip;
    theSceneD = theScene;
    
    theTopBarD = [TopMenu getTopBar];
    
    shipNameD = [[UILabel alloc]initWithFrame:CGRectMake(backgroundNode.size.width/3, backgroundNode.size.height/4.5, backgroundNode.size.width/1.5, backgroundNode.size.height/10)];
    shipNameD.text = [NSString stringWithFormat:@"%@: %@",theShip.shipBrand, theShip.shipModel];
    shipNameD.font = [UIFont fontWithName:@"OratorStd" size:24];
    shipNameD.adjustsFontSizeToFitWidth = TRUE;
    shipNameD.textAlignment = NSTextAlignmentCenter;
    shipNameD.textColor = [UIColor whiteColor];
    shipNameD.alpha = 0.8;
    
    availableDefenseEquip= [[UITableView alloc]initWithFrame:CGRectMake(backgroundNode.size.width/1.5, backgroundNode.size.height/3, backgroundNode.size.width/2.2, backgroundNode.size.height/1.3)];
    availableDefenseEquip.backgroundColor = [UIColor clearColor];
    availableDefenseEquip.separatorColor = [UIColor colorWithRed:0 green:0.659 blue:0.91 alpha:1]; //#00a8e8
    availableDefenseEquip.tag = 3;
    availableDefenseEquip.tableFooterView = [UIView new];
    [availableDefenseEquip setSeparatorInset:UIEdgeInsetsMake(0, 55, 0, 0)];
    
    shipDefenseSpots = [[UITableView alloc]initWithFrame:CGRectMake(backgroundNode.size.width/5, backgroundNode.size.height/3, backgroundNode.size.width/2.2, backgroundNode.size.height/1.3)];
    shipDefenseSpots.backgroundColor = [UIColor clearColor];
    shipDefenseSpots.separatorColor = [UIColor colorWithRed:0 green:0.659 blue:0.91 alpha:1]; //#00a8e8
    shipDefenseSpots.tag = 1;
    shipDefenseSpots.tableFooterView = [UIView new];
    [shipDefenseSpots setSeparatorInset:UIEdgeInsetsMake(0, 55, 0, 0)];
    
    saveButtonDD = [SKSpriteNode spriteNodeWithImageNamed:@"saveDark"];
    saveButtonDD.size = CGSizeMake(theTopBarD.frame.size.width/8, theTopBarD.size.height/2.9);
    saveButtonDD.position = CGPointMake(theTopBarD.size.width/10-saveButtonDD.size.width, -saveButtonDD.size.height/1.1);
    saveButtonDD.zPosition = 7;
    saveButtonDD.alpha = 0.75;
    
    saveButtonAD = [SKSpriteNode spriteNodeWithImageNamed:@"saveLight"];
    saveButtonAD.size = CGSizeMake(theTopBarD.frame.size.width/8, theTopBarD.size.height/2.9);
    saveButtonAD.position = CGPointMake(theTopBarD.size.width/10-saveButtonDD.size.width, -saveButtonDD.size.height/1.1);
    saveButtonAD.name = @"save";
    saveButtonAD.zPosition = 7;
    saveButtonAD.alpha = 0.75;
    
    loadButtonDD = [SKSpriteNode spriteNodeWithImageNamed:@"loadDark"];
    loadButtonDD.size = CGSizeMake(theTopBarD.frame.size.width/8, theTopBarD.size.height/2.9);
    loadButtonDD.position = CGPointMake(theTopBarD.size.width/4.5-loadButtonDD.size.width, -loadButtonDD.size.height/1.1);
    loadButtonDD.zPosition =7;
    loadButtonDD.alpha = 0.75;
    
    loadButtonAD = [SKSpriteNode spriteNodeWithImageNamed:@"loadLight"];
    loadButtonAD.size = CGSizeMake(theTopBarD.frame.size.width/8, theTopBarD.size.height/2.9);
    loadButtonAD.position = CGPointMake(theTopBarD.size.width/4.5-loadButtonDD.size.width, -loadButtonDD.size.height/1.1);
    loadButtonAD.zPosition =7;
    loadButtonAD.name = @"load";
    loadButtonAD.alpha = 0.75;
    
    deleteButtonDD = [SKSpriteNode spriteNodeWithImageNamed:@"deleteDark"];
    deleteButtonDD.size = CGSizeMake(theTopBarD.frame.size.width/8, theTopBarD.size.height/2.9);
    deleteButtonDD.position = CGPointMake(theTopBarD.size.width/2.91-deleteButtonDD.size.width, -deleteButtonDD.size.height/1.1);
    deleteButtonDD.zPosition =7;
    deleteButtonDD.alpha = 0.75;
    
    deleteButtonAD = [SKSpriteNode spriteNodeWithImageNamed:@"deleteLight"];
    deleteButtonAD.size = CGSizeMake(theTopBarD.frame.size.width/8, theTopBarD.size.height/2.9);
    deleteButtonAD.position = CGPointMake(theTopBarD.size.width/2.91-deleteButtonDD.size.width, -deleteButtonDD.size.height/1.1);
    deleteButtonAD.zPosition =7;
    deleteButtonAD.name = @"delete";
    deleteButtonAD.alpha = 0.75;
    
    simButtonDD = [SKSpriteNode spriteNodeWithImageNamed:@"testDark"];
    simButtonDD.size = CGSizeMake(theTopBarD.frame.size.width/8, theTopBarD.size.height/2.9);
    simButtonDD.position = CGPointMake(theTopBarD.size.width/1.8-simButtonDD.size.width, -simButtonDD.size.height/1.1);
    simButtonDD.zPosition =7;
    simButtonDD.alpha = 0.75;
    
    simButtonAD = [SKSpriteNode spriteNodeWithImageNamed:@"testLight"];
    simButtonAD.size = CGSizeMake(theTopBarD.frame.size.width/8, theTopBarD.size.height/2.9);
    simButtonAD.position = CGPointMake(theTopBarD.size.width/1.8-simButtonDD.size.width, -simButtonDD.size.height/1.1);
    simButtonAD.zPosition =7;
    simButtonAD.name = @"sim";
    simButtonAD.alpha = 0.75;
    
    [theTopBarD addChild:loadButtonDD];
    [theTopBarD addChild:saveButtonDD];
    [theTopBarD addChild:deleteButtonDD];
    [theTopBarD addChild:simButtonDD];
    
    NSUserDefaults * shipSavedD = [NSUserDefaults standardUserDefaults];
    NSDictionary * defSaved = [shipSavedD rm_customObjectForKey:[NSString stringWithFormat:@"%@Defense",theShip.shipModel]];
    if (defSaved != nil) {
        [loadButtonDD removeFromParent];
        [theTopBarD addChild:loadButtonAD];
        loadButtonActive = 1;
        
    }

    //-----------///-------------//
    [[DefenseTab initDefenseTab]setDelegate];
    [theScene.view addSubview:availableDefenseEquip];
    [theScene.view addSubview:shipDefenseSpots];
    [theScene.view addSubview:shipNameD];
}

+(void)removeDefenseTab{
    [availableDefenseEquip removeFromSuperview];
    [shipDefenseSpots removeFromSuperview];
    [shipNameD removeFromSuperview];
    if (loadButtonActive == 1) {
        [loadButtonAD removeFromParent];
        loadButtonActive = 0;
    }else{
        [loadButtonDD removeFromParent];
        loadButtonActive = 0;
    }
    if (saveButtonActive == 1) {
        [saveButtonAD removeFromParent];
        saveButtonActive = 0;
    }else{
        [saveButtonDD removeFromParent];
        saveButtonActive = 0;
    }
    if (deleteButtonActive == 1) {
        [deleteButtonAD removeFromParent];
        deleteButtonActive = 0;
    }else{
        [deleteButtonDD removeFromParent];
        deleteButtonActive = 0;
    }
    if (simButtonActive == 1) {
        [simButtonAD removeFromParent];
        simButtonActive = 0;
    }else{
        [simButtonDD removeFromParent];
        simButtonActive = 0;
    }
}

-(void)setDelegate{
    DefenseDelegate * defDelegate = [DefenseDelegate initDefenseDelegateWithShip:defship theAvalibleDefense:aDefenseEquip theScene:theSceneD];
    availableDefenseEquip.delegate = defDelegate;
    availableDefenseEquip.dataSource = defDelegate;
    shipDefenseSpots.delegate = defDelegate;
    shipDefenseSpots.dataSource = defDelegate;
}
+(void)reloadDefenseShipMounts:(NSIndexPath *)unselectPath selectPath:(NSIndexPath *)selectPath{
    [shipDefenseSpots reloadData];
    [availableDefenseEquip deselectRowAtIndexPath:unselectPath animated:YES];
    [shipDefenseSpots selectRowAtIndexPath:selectPath animated:NO scrollPosition:NO];
}
+(void)activateSaveButton{
    if (saveButtonActive == 0) {
        [saveButtonDD removeFromParent];
        [theTopBarD addChild:saveButtonAD];
        saveButtonActive = 1;
    }
    
}
+(void)deactivateSaveButton{
    if (saveButtonActive == 1) {
        [saveButtonAD removeFromParent];
        [theTopBarD addChild:saveButtonDD];
        saveButtonActive = 0;
    }
    
}
+(void)activateLoadButton{
    if (loadButtonActive == 0) {
        [loadButtonDD removeFromParent];
        [theTopBarD addChild:loadButtonAD];
        loadButtonActive = 1;
    }
    
}
+(void)deactivateLoadButton{
    if (loadButtonActive == 1) {
        [loadButtonAD removeFromParent];
        [theTopBarD addChild:loadButtonDD];
        loadButtonActive = 0;
    }
    
}
+(void)activateDeleteButton{
    if (deleteButtonActive == 0) {
        [deleteButtonDD removeFromParent];
        [theTopBarD addChild:deleteButtonAD];
        deleteButtonActive = 1;
    }
    
}
+(void)deactivateDeleteButton{
    if (deleteButtonActive == 1) {
        [deleteButtonAD removeFromParent];
        [theTopBarD addChild:deleteButtonDD];
        deleteButtonActive = 0;
    }
    
}
+(void)activateSimButton{
    if (simButtonActive == 0) {
        [simButtonDD removeFromParent];
        [theTopBarD addChild:simButtonAD];
        simButtonActive = 1;
    }
    
}
+(void)deactivateSimButton{
    if (simButtonActive == 1) {
        [simButtonAD removeFromParent];
        [theTopBarD addChild:simButtonDD];
        simButtonActive = 0;
    }
    
}
+(void)hideView:(BOOL)isHidden{
    availableDefenseEquip.hidden = isHidden;
    shipDefenseSpots.hidden = isHidden;
    shipNameD.hidden = isHidden;
}

@end
