//
//  TopMenu.h
//  SCAR
//
//  Created by Alexzander Jaggi on 3/31/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

#import "FirebaseData.h"
#import "Ships.h"
#import "EquipmentView.h"
#import "GameScene.h"

@interface TopMenu : NSObject <UITableViewDelegate, UITableViewDataSource>


+(void)presentTopMenu:(SKScene *)theScene viewBackground:(SKSpriteNode *)backgroundNode;
+(void)removeTopMenu;
+(void)presentSelectShipMenu:(SKScene *)theScene;
+(void)removeSelectShipMenu;
+(SKSpriteNode *)getTopBar;
+(void)hideView:(BOOL)isHidden;
@end
