//
//  SystemTab.m
//  SCAR
//
//  Created by Alexzander Jaggi on 4/9/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import "SystemTab.h"

UITableView * availableSystemEquip;
UITextView * descText;
NSDictionary * systemEquip;
NSArray * sectionHeaders;
NSArray * theKeys;
SKScene * theSceneS;

@implementation SystemTab

+(SystemTab *)initSystemTab{
    static SystemTab * theSystemTab;
    @synchronized (self) {
        if (!theSystemTab) {
            theSystemTab = [[self alloc]init];
        }
    }
    return theSystemTab;
}

+(void)presentSystemTab:(SKScene *)theScene viewBackground:(SKSpriteNode *)backgroundNode systemEquip:(NSDictionary *)theEquip{
    theSceneS = theScene;
    systemEquip = theEquip;
    sectionHeaders = @[@"Coolers",@"Power Plants"];
    theKeys = @[@"coolers",@"power"];
    
    availableSystemEquip= [[UITableView alloc]initWithFrame:CGRectMake(backgroundNode.size.width/1.55, backgroundNode.size.height/3, backgroundNode.size.width/2.1, backgroundNode.size.height/1.3)];
    availableSystemEquip.backgroundColor = [UIColor clearColor];
    availableSystemEquip.separatorColor = [UIColor colorWithRed:0 green:0.659 blue:0.91 alpha:1]; //#00a8e8
    availableSystemEquip.tableFooterView = [UIView new];
    [availableSystemEquip setSeparatorInset:UIEdgeInsetsMake(0, 55, 0, 0)];
    
    descText = [[UITextView alloc]initWithFrame:CGRectMake(backgroundNode.size.width/5, backgroundNode.size.height/3, backgroundNode.size.width/2.3, backgroundNode.size.height/1.3)];
    descText.backgroundColor = [UIColor clearColor];
    descText.font = [UIFont fontWithName:@"OratorStd" size:14];
    descText.textColor = [UIColor whiteColor];
    descText.textAlignment = NSTextAlignmentCenter;
    descText.editable = NO;
    descText.layer.borderWidth = 1;
    descText.layer.borderColor = [[UIColor colorWithRed:0 green:0.659 blue:0.91 alpha:0.4]CGColor]; //#00a8e8
    descText.layer.cornerRadius = 10;
    descText.alpha = 0.8;
    
    
    //-----------///-------------//
    
    [[SystemTab initSystemTab]setDelegate];
    [theScene.view addSubview:availableSystemEquip];
    [theScene.view addSubview:descText];
}

+(void)removeSystemTab{
    [availableSystemEquip removeFromSuperview];
    [descText removeFromSuperview];
}

-(void)setDelegate{
    availableSystemEquip.delegate = self;
    availableSystemEquip.dataSource = self;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray * theEquipArray = [systemEquip objectForKey:[theKeys objectAtIndex:section]];
    
    return theEquipArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    NSString * theArrayKeys = [theKeys objectAtIndex:indexPath.section];
    NSArray * theItems = [systemEquip objectForKey:theArrayKeys];
    SystemEquipment * theSysEquip = [theItems objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    
    //Table Cell
    
    cell.textLabel.text = theSysEquip.itemName;
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont fontWithName:@"OratorStd" size:10];
    cell.textLabel.alpha = 0.8;
    cell.detailTextLabel.alpha = 0.8;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",theSysEquip.itemAblility];
    cell.detailTextLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.font = [UIFont fontWithName:@"OratorStd" size:10];
    cell.selectedBackgroundView = [UIView new];
    cell.selectedBackgroundView.layer.cornerRadius = 10;
    cell.selectedBackgroundView.alpha = 0.5;
    cell.selectedBackgroundView.layer.masksToBounds = YES;
    cell.selectedBackgroundView.backgroundColor = [UIColor colorWithRed:0 green:0.659 blue:0.91 alpha:0.4]; //#00a8e8
    
    return cell;
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return sectionHeaders.count;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [sectionHeaders objectAtIndex:section];
}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    
    view.tintColor = [UIColor colorWithRed:0 green:0.494 blue:0.655 alpha:0.5]; //#007ea7
    view.layer.cornerRadius = 10;
    view.layer.masksToBounds = YES;
    
    UITableViewHeaderFooterView * header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setFont:[UIFont fontWithName:@"OratorStd" size:10]];
    [header.textLabel setTextColor:[UIColor colorWithRed:0 green:0.204 blue:0.349 alpha:1]]; //#003459
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SKAction * clickedSound = [SKAction playSoundFileNamed:@"click2.wav" waitForCompletion:NO];
    NSString * theArrayKeys = [theKeys objectAtIndex:indexPath.section];
    NSArray * theItems = [systemEquip objectForKey:theArrayKeys];
    SystemEquipment * theSysEquip = [theItems objectAtIndex:indexPath.row];
    descText.text = [NSString stringWithFormat:@"Description \n \n %@",theSysEquip.itemDescription];
    if ([GameScene getAudioPOS] == 0) {
        [theSceneS runAction:clickedSound];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}
@end
