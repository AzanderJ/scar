//
//  GameScene.h
//  SCAR
//

//  Copyright (c) 2016 Alexzander Jaggi. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <UIKit/UIKit.h>

#import "Asteroids.h"
#import "EquipmentView.h"
#import "MapView.h"
#import "RedditView.h"
#import "FirebaseData.h"
#import "ShipEquipmentDelegate.h"
#import "UserShip.h"
#import "EnemyShip.h"
#import "WeaponsFire.h"
#import "Credits.h"
#import "Reachability.h"
#import "Settings.h"
#import "NSUserDefaults+RMSaveCustomObject.h"



@interface GameScene : SKScene <SKPhysicsContactDelegate>

+(void)setShipSelectorPOS:(int)isOpen;
+(void)tab1POS:(int)pos;
+(void)tab2POS:(int)pos;
+(void)tab3POS:(int)pos;
+(void)tab4POS:(int)pos;
+(void)hideView:(BOOL)isHidden;
+(void)activateSim:(Ships *)thePlayerShip whatSim:(int)simType playerDPS:(int)playerDPS playerShield:(int)playerShield;
+(int)getAudioPOS;

@end
