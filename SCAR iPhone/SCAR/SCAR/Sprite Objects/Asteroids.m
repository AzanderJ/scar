//
//  Asteroids.m
//  SCAR
//
//  Created by Alexzander Jaggi on 3/22/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import "Asteroids.h"

@implementation Asteroids

Asteroids * asteroids;

+(Asteroids *)initAsteroids:(SKScene *)theScene{
    asteroids = [Asteroids spriteNodeWithImageNamed:@"asteroid"];
    asteroids.zPosition = -1;
//    asteroids.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:asteroids.frame.size.width/2];
//    asteroids.physicsBody.restitution = 1;
//    asteroids.physicsBody.mass = 20;

    [theScene addChild:asteroids];
    [asteroids moveAsteroids:theScene];
    return asteroids;
}

-(void)moveAsteroids: (SKScene *)theScene {
    float randomY = [self randomValueBetween:0.0 andValue:theScene.frame.size.height];
    float randomDuration = [self randomValueBetween:10.0 andValue:50];
    float randomScale = [self randomValueBetween:0.5 andValue:1.2];
    asteroids.xScale = randomScale;
    asteroids.yScale = randomScale;
    asteroids.position = CGPointMake(theScene.frame.size.width + asteroids.size.width/2, randomY);
    CGPoint location = CGPointMake(- theScene.frame.size.width - asteroids.size.width, randomY);
    SKAction * moveAsteroid = [SKAction moveTo:location duration:randomDuration];
    [asteroids runAction:moveAsteroid];
}

-(float)randomValueBetween:(float)low andValue:(float)high{
    return (((float)arc4random()/0xFFFFFFFFu)*(high-low))+low;
}

@end
