//
//  MapView.m
//  SCAR
//
//  Created by Alexzander Jaggi on 3/24/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import "MapView.h"

@implementation MapView

SKSpriteNode * backgroundNodeMap;
UIWebView * mapViewer;
UIActivityIndicatorView * loadingIndicator;
SKSpriteNode * backgroundMidM;
SKSpriteNode * backgroundBottomM;
SKSpriteNode * scanLineM;
NSArray * backgroundBottomTexturesM;
NSArray * backgroundMidTexturesM;
SKAction * runScanSeqM;
SKAction * scanM;
SKEmitterNode * emitterEffectM;
SKSpriteNode * interactiveMap;
SKSpriteNode * flatMapToggle;
UIImageView * mapImgView;
NSURLRequest * urlRequest;
int openTab;

+(MapView *)initMapView{
    static MapView * theMapView;
    @synchronized (self) {
        if (!theMapView) {
            theMapView = [[self alloc]init];
        }
    }
    return theMapView;
}

+(void)openMapView:(SKScene *)theScene{
    
    openTab = 0;
    
    backgroundNodeMap = [SKSpriteNode spriteNodeWithImageNamed:@"backgroundTop"];
    backgroundNodeMap.zPosition = 2;
    backgroundNodeMap.alpha = 0.75;
    backgroundNodeMap.size = CGSizeMake(theScene.frame.size.width/1.20, theScene.frame.size.height/1.2);
    backgroundNodeMap.position = CGPointMake(theScene.size.width - backgroundNodeMap.size.width/1.85, theScene.size.height/2.2);
    
    NSMutableArray * midFrames = [[NSMutableArray alloc]init];
    SKTextureAtlas * midTextureAtlas = [SKTextureAtlas atlasNamed:@"mid"];
    long midTextureCount = midTextureAtlas.textureNames.count;
    for (int i=1; i <= midTextureCount; i++) {
        NSString * midTextureName = [NSString stringWithFormat:@"midLayer%d",i];
        SKTexture * midTempTexture = [midTextureAtlas textureNamed:midTextureName];
        [midFrames addObject:midTempTexture];
    }
    backgroundMidTexturesM = [[NSArray alloc]initWithArray:midFrames];
    
    backgroundMidM = [SKSpriteNode spriteNodeWithImageNamed:@"midLayer1"];
    backgroundMidM.size = CGSizeMake(theScene.frame.size.width/1.20, theScene.frame.size.height/1.2);
    backgroundMidM.position = CGPointMake(theScene.size.width - backgroundNodeMap.size.width/1.85, theScene.size.height/2.2);
    backgroundMidM.zPosition = 1;
    backgroundMidM.alpha = 0.3;
    
    NSMutableArray * bottomFrames = [[NSMutableArray alloc]init];
    SKTextureAtlas * bottomTextureAtlas = [SKTextureAtlas atlasNamed:@"bottom"];
    long bottomTextureCount = bottomTextureAtlas.textureNames.count;
    for (int j=1; j <= bottomTextureCount; j++) {
        NSString * bottomTextureName = [NSString stringWithFormat:@"bottomLayer%d",j];
        SKTexture * bottomTempTexture = [bottomTextureAtlas textureNamed:bottomTextureName];
        [bottomFrames addObject:bottomTempTexture];
    }
    backgroundBottomTexturesM = [[NSArray alloc]initWithArray:bottomFrames];
    
    backgroundBottomM = [SKSpriteNode spriteNodeWithTexture:bottomFrames[0]];
    backgroundBottomM.size = CGSizeMake(theScene.frame.size.width/1.20, theScene.frame.size.height/1.2);
    backgroundBottomM.position = CGPointMake(theScene.size.width - backgroundNodeMap.size.width/1.85, theScene.size.height/2.2);
    backgroundBottomM.zPosition = 0;
    backgroundBottomM.alpha = 0.5;
    
    //Scan Line & dust effect
    scanLineM = [SKSpriteNode spriteNodeWithImageNamed:@"scanLine"];
    scanLineM.zPosition = 0;
    scanLineM.alpha = 0.2;
    scanLineM.size = CGSizeMake(backgroundNodeMap.frame.size.width, scanLineM.frame.size.height/20);
    scanLineM.position = CGPointMake(backgroundNodeMap.frame.size.width/1.52, backgroundNodeMap.frame.size.height/50);
    scanLineM.hidden = FALSE;
    
    scanM = [SKAction moveTo:CGPointMake(backgroundNodeMap.frame.size.width/1.52, backgroundNodeMap.frame.size.height) duration:5];
    SKAction * resetScan = [SKAction moveTo:CGPointMake(backgroundNodeMap.frame.size.width/1.52, backgroundNodeMap.frame.size.height/50) duration:0];
    runScanSeqM = [SKAction sequence:@[scanM,resetScan]];
    
    emitterEffectM = [SKEmitterNode nodeWithFileNamed:@"ScanParticals"];
    emitterEffectM.particlePositionRange = CGVectorMake(backgroundNodeMap.frame.size.width, backgroundNodeMap.frame.size.height/1.5);
    emitterEffectM.zPosition = 0;
    
    [backgroundMidM runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:backgroundMidTexturesM timePerFrame: 0.08 resize:NO restore:YES]]withKey:@"midRunning"];
    
    [backgroundBottomM runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:backgroundBottomTexturesM timePerFrame: 0.1 resize:NO restore:YES]]withKey:@"bottomRunning"];
    
    [scanLineM runAction:runScanSeqM completion:^{
        scanLineM.hidden = TRUE;
    }];
    
    
    //Map URL & Request
    NSString * mapURLstring = @"http://leeft.eu/starcitizen/";
    NSURL * mapURL = [NSURL URLWithString:mapURLstring];
    urlRequest = [NSURLRequest requestWithURL:mapURL cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30];
    
    //UIWebView for map
    mapViewer = [[UIWebView alloc]initWithFrame:CGRectMake(backgroundNodeMap.size.width/5.5, theScene.size.height - backgroundNodeMap.size.height, backgroundNodeMap.size.width/1.05, backgroundNodeMap.size.height/1.1)];
    mapViewer.alpha = 0.7;
    mapViewer.scalesPageToFit = YES;
    mapViewer.backgroundColor = [UIColor clearColor];
    mapViewer.opaque = NO;
    mapViewer.layer.cornerRadius = 20;
    mapViewer.allowsInlineMediaPlayback = NO;
    
    //Loading Indicator
    loadingIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    loadingIndicator.center = CGPointMake(mapViewer.center.x,mapViewer.center.y);
    
    //tabs
    interactiveMap = [SKSpriteNode spriteNodeWithImageNamed:@"3dmapActive"];
    interactiveMap.size = CGSizeMake(backgroundNodeMap.size.width/6, backgroundNodeMap.size.height/10);
    interactiveMap.zPosition = 2;
    interactiveMap.alpha  = 1.25;
    interactiveMap.position = CGPointMake(backgroundNodeMap.size.width/2.4 - backgroundNodeMap.size.width/1.22, backgroundNodeMap.size.height/1.87);
    interactiveMap.name = @"interactiveMap";
    
    flatMapToggle = [SKSpriteNode spriteNodeWithImageNamed:@"2dMapInactive"];
    flatMapToggle.size = CGSizeMake(backgroundNodeMap.size.width/6, backgroundNodeMap.size.height/10);
    flatMapToggle.zPosition = 2;
    flatMapToggle.alpha = 1.25;
    flatMapToggle.position = CGPointMake(backgroundNodeMap.size.width/1.72 - backgroundNodeMap.size.width/1.22, backgroundNodeMap.size.height/1.87);
    flatMapToggle.name = @"flatMapToggle";
    
    //imageview
    
    mapImgView = [[UIImageView alloc]initWithFrame:CGRectMake(backgroundNodeMap.size.width/5.5, theScene.size.height - backgroundNodeMap.size.height, backgroundNodeMap.size.width/1.05, backgroundNodeMap.size.height/1.05)];
    mapImgView.image = [UIImage imageNamed:@"galaxyMap"];
    mapImgView.contentMode = UIViewContentModeScaleAspectFit;
    mapImgView.hidden = YES;
    
    //--------------///-----------------//
    
    [theScene addChild:backgroundBottomM];
    [theScene addChild:backgroundMidM];
    [theScene addChild:backgroundNodeMap];
    [backgroundNodeMap addChild:emitterEffectM];
    [theScene addChild:scanLineM];
    [theScene.view addSubview:mapViewer];
    [[MapView initMapView]loadDelegate];
    [theScene.view addSubview:loadingIndicator];
    [loadingIndicator startAnimating];
    [mapViewer loadRequest:urlRequest];
    [mapViewer setClipsToBounds:YES];
    [backgroundNodeMap addChild:interactiveMap];
    [backgroundNodeMap addChild:flatMapToggle];
    [theScene.view addSubview:mapImgView];
}


+(void)closeMapView{
    [backgroundNodeMap removeFromParent];
    [backgroundMidM removeFromParent];
    [backgroundBottomM removeFromParent];
    [scanLineM removeFromParent];
    [mapViewer removeFromSuperview];
    [loadingIndicator stopAnimating];
    [loadingIndicator removeFromSuperview];
    [mapImgView removeFromSuperview];
}

-(void)loadDelegate{
    mapViewer.delegate = self;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [loadingIndicator stopAnimating];
    [loadingIndicator removeFromSuperview];

}

+(void)activateMap:(int)whatMap{
    switch (whatMap) {
        case 1:
            if (openTab == 0) {
                interactiveMap.texture = [SKTexture textureWithImageNamed:@"3dmapInactive"];
                flatMapToggle.texture = [SKTexture textureWithImageNamed:@"2dmapActive"];
                openTab = 1;
                mapViewer.hidden = YES;
                loadingIndicator.hidden = YES;
                mapImgView.hidden = NO;
                backgroundBottomM.alpha = 0.9; //changed the 2d map BG so the dark text is easier to read
                
            }
            break;
        case 0:
            if (openTab == 1) {
                interactiveMap.texture = [SKTexture textureWithImageNamed:@"3dmapActive"];
                flatMapToggle.texture = [SKTexture textureWithImageNamed:@"2dMapInactive"];
                openTab = 0;
                mapViewer.hidden = NO;
                loadingIndicator.hidden = NO;
                mapImgView.hidden = YES;
                backgroundBottomM.alpha = 0.5;
            }else{
                [mapViewer loadRequest:urlRequest];
            }
            break;
            
        default:
            break;
    }
}

@end
