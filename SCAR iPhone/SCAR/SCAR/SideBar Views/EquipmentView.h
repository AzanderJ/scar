//
//  EquipmentView.h
//  SCAR
//
//  Created by Alexzander Jaggi on 3/23/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "GeneralTab.h"
#import "WeaponTab.h"
#import "DefenseTab.h"
#import "SystemTab.h"
#import "Ships.h"
#import "Weapons.h"
#import "DefenseEquipment.h"
#import "SystemEquipment.h"
#import "TopMenu.h"
#import "FirebaseData.h"

@interface EquipmentView : NSObject

@property(nonatomic)GeneralTab * generalTab;

//Equipment tab delegate
+(void)openEquipmentView:(SKScene *)theScene;
+(void)closeEquipmentView;

//Main Viewer
+(void)presentDataViews:(Ships *)ship;

//Tabs
+(void)activateGeneralTab;
+(void)deactivateGeneralTab;

+(void)activateWeaponTab;
+(void)deactivateWeaponTab;

+(void)activateDefenseTab;
+(void)deactivateDefenseTab;

+(void)activateSystemTab;
+(void)deactivateSystemTab;

+(void)activeShipSelector;
+(void)deactivateShipSelector;

+(void)hideView:(BOOL)isHidden;

@end
