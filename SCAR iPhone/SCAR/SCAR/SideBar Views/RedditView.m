//
//  RedditView.m
//  SCAR
//
//  Created by Alexzander Jaggi on 3/24/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import "RedditView.h"

SKSpriteNode * backgroundNodeReddit;
SKSpriteNode * backgroundMidR;
SKSpriteNode * backgroundBottomR;
SKSpriteNode * scanLineR;
UITableView * redditTableView;
NSMutableArray * newPosts;
NSString * theSubUrlString;
NSArray * backgroundBottomTexturesR;
NSArray * backgroundMidTexturesR;
SKAction * runScanSeqR;
SKAction * scanR;
SKEmitterNode * emitterEffectR;
UIWebView * webViewR;
SKScene * theSceneR;
SKSpriteNode * doneButtonR;
UIActivityIndicatorView * loadingIndicatorR;
NSURL * noURL;
NSURLRequest * theUrlRequest;
UITextView * descTextR;

@implementation RedditView

+(RedditView *)initRedditView{
    static RedditView * redditView;
    @synchronized (self) {
        if (!redditView) {
            redditView = [[self alloc]init];
        }
    }
    return redditView;
}

+(void)openRedditView:(SKScene *)theScene{
    
    newPosts = [[NSMutableArray alloc]init];
    theSceneR = theScene;
    noURL = [NSURL URLWithString:@""];

    //view background
    backgroundNodeReddit = [SKSpriteNode spriteNodeWithImageNamed:@"backgroundTop"];
    backgroundNodeReddit.zPosition = 2;
    backgroundNodeReddit.alpha = 0.75;
    backgroundNodeReddit.size = CGSizeMake(theScene.frame.size.width/1.20, theScene.frame.size.height/1.2);
    backgroundNodeReddit.position = CGPointMake(theScene.size.width - backgroundNodeReddit.size.width/1.85, theScene.size.height/2.2);
    
    NSMutableArray * midFrames = [[NSMutableArray alloc]init];
    SKTextureAtlas * midTextureAtlas = [SKTextureAtlas atlasNamed:@"mid"];
    long midTextureCount = midTextureAtlas.textureNames.count;
    for (int i=1; i <= midTextureCount; i++) {
        NSString * midTextureName = [NSString stringWithFormat:@"midLayer%d",i];
        SKTexture * midTempTexture = [midTextureAtlas textureNamed:midTextureName];
        [midFrames addObject:midTempTexture];
    }
    backgroundMidTexturesR = [[NSArray alloc]initWithArray:midFrames];
    
    backgroundMidR = [SKSpriteNode spriteNodeWithImageNamed:@"midLayer1"];
    backgroundMidR.size = CGSizeMake(theScene.frame.size.width/1.20, theScene.frame.size.height/1.2);
    backgroundMidR.position = CGPointMake(theScene.size.width - backgroundNodeReddit.size.width/1.85, theScene.size.height/2.2);
    backgroundMidR.zPosition = 1;
    backgroundMidR.alpha = 0.3;
    
    NSMutableArray * bottomFrames = [[NSMutableArray alloc]init];
    SKTextureAtlas * bottomTextureAtlas = [SKTextureAtlas atlasNamed:@"bottom"];
    long bottomTextureCount = bottomTextureAtlas.textureNames.count;
    for (int j=1; j <= bottomTextureCount; j++) {
        NSString * bottomTextureName = [NSString stringWithFormat:@"bottomLayer%d",j];
        SKTexture * bottomTempTexture = [bottomTextureAtlas textureNamed:bottomTextureName];
        [bottomFrames addObject:bottomTempTexture];
    }
    backgroundBottomTexturesR = [[NSArray alloc]initWithArray:bottomFrames];
    
    backgroundBottomR = [SKSpriteNode spriteNodeWithTexture:bottomFrames[0]];
    backgroundBottomR.size = CGSizeMake(theScene.frame.size.width/1.20, theScene.frame.size.height/1.2);
    backgroundBottomR.position = CGPointMake(theScene.size.width - backgroundNodeReddit.size.width/1.85, theScene.size.height/2.2);
    backgroundBottomR.zPosition = 0;
    backgroundBottomR.alpha = 0.5;
    
    //Scan Line & dust effect
    scanLineR = [SKSpriteNode spriteNodeWithImageNamed:@"scanLine"];
    scanLineR.zPosition = 0;
    scanLineR.alpha = 0.2;
    scanLineR.size = CGSizeMake(backgroundNodeReddit.frame.size.width, scanLineR.frame.size.height/20);
    scanLineR.position = CGPointMake(backgroundNodeReddit.frame.size.width/1.52, backgroundNodeReddit.frame.size.height/50);
    scanLineR.hidden = FALSE;
    
    scanR = [SKAction moveTo:CGPointMake(backgroundNodeReddit.frame.size.width/1.52, backgroundNodeReddit.frame.size.height) duration:5];
    SKAction * resetScan = [SKAction moveTo:CGPointMake(backgroundNodeReddit.frame.size.width/1.52, backgroundNodeReddit.frame.size.height/50) duration:0];
    runScanSeqR = [SKAction sequence:@[scanR,resetScan]];
    
    emitterEffectR = [SKEmitterNode nodeWithFileNamed:@"ScanParticals"];
    emitterEffectR.particlePositionRange = CGVectorMake(backgroundNodeReddit.frame.size.width, backgroundNodeReddit.frame.size.height/1.5);
    emitterEffectR.zPosition = 0;
    
    [backgroundMidR runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:backgroundMidTexturesR timePerFrame: 0.08 resize:NO restore:YES]]withKey:@"midRunning"];
    
    [backgroundBottomR runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:backgroundBottomTexturesR timePerFrame: 0.1 resize:NO restore:YES]]withKey:@"bottomRunning"];
    
    [scanLineR runAction:runScanSeqR completion:^{
        scanLineR.hidden = TRUE;
    }];
    
    //tableview
    redditTableView = [[UITableView alloc]initWithFrame:CGRectMake(backgroundNodeReddit.size.width/5, backgroundNodeReddit.size.height*1.1, backgroundNodeReddit.size.width/1.1, -backgroundNodeReddit.size.height/1.1)];
    redditTableView.backgroundColor = [UIColor clearColor];
    redditTableView.separatorColor = [UIColor colorWithRed:0 green:0.659 blue:0.91 alpha:1]; //#00a8e8
    
    doneButtonR = [SKSpriteNode spriteNodeWithImageNamed:@"doneButton"];
    doneButtonR.yScale = 0.2;
    doneButtonR.xScale = 0.2;
    doneButtonR.position = CGPointMake(backgroundNodeReddit.size.width +doneButtonR.size.width/8, theScene.frame.size.height-doneButtonR.frame.size.height/2);
    doneButtonR.name = @"doneR";
    doneButtonR.hidden = YES;
    
    webViewR = [[UIWebView alloc]initWithFrame:CGRectMake(backgroundNodeReddit.size.width/5.5, theSceneR.size.height - backgroundNodeReddit.size.height, backgroundNodeReddit.size.width/1.05, backgroundNodeReddit.size.height/1.1)];
    webViewR.backgroundColor = [UIColor clearColor];
    webViewR.opaque = NO;
    webViewR.layer.cornerRadius = 20;
    webViewR.hidden = YES;
    
    descTextR = [[UITextView alloc]initWithFrame:CGRectMake(backgroundNodeReddit.size.width/5, backgroundNodeReddit.frame.size.height/50, backgroundNodeReddit.size.width/2, backgroundNodeReddit.size.height/10)];
    descTextR.backgroundColor = [UIColor colorWithRed:0.325 green:0.396 blue:0.831 alpha:0.65]; //#5365d4
    descTextR.font = [UIFont fontWithName:@"OratorStd" size:14];
    descTextR.textColor = [UIColor whiteColor];
    descTextR.textAlignment = NSTextAlignmentCenter;
    descTextR.editable = NO;
    descTextR.layer.cornerRadius = 10;
    descTextR.text = @"Reddit.com/r/StarCitizenAR";
    descTextR.scrollEnabled = FALSE;
    descTextR.hidden = NO;
    descTextR.alpha = 0.9;
    
    //Loading Indicator
    loadingIndicatorR = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    loadingIndicatorR.center = CGPointMake(webViewR.center.x,webViewR.center.y);
    
    //--------------///-----------------//
    [theScene addChild:backgroundBottomR];
    [theScene addChild:backgroundMidR];
    [theScene addChild:backgroundNodeReddit];
    [backgroundNodeReddit addChild:emitterEffectR];
    [theScene addChild:scanLineR];
    [theScene.view addSubview:redditTableView];
    [[RedditView initRedditView]getNetwork:@"StarCitizenAR"];
    [theSceneR addChild:doneButtonR];
    [theSceneR.view addSubview:descTextR];
}

+(void)closeRedditView{
    [backgroundNodeReddit removeFromParent];
    [backgroundBottomR removeFromParent];
    [backgroundMidR removeFromParent];
    [scanLineR removeFromParent];
    [redditTableView removeFromSuperview];
    [webViewR removeFromSuperview];
    [doneButtonR removeFromParent];
    theUrlRequest = [NSURLRequest requestWithURL:noURL cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:1];
    [webViewR loadRequest:theUrlRequest];
    [descTextR removeFromSuperview];
}

// Get JSON from reddit
-(void)getNetwork:(NSString*)theSubReddit{
    redditTableView.delegate = self;
    redditTableView.dataSource = self;
    webViewR.delegate = self;
    theSubUrlString = [NSString stringWithFormat:@"http://reddit.com/r/%@.json", theSubReddit];
    NSURL * theSub = [NSURL URLWithString:theSubUrlString];
    NSData * thePosts = [NSData dataWithContentsOfURL:theSub];
    if (thePosts !=nil) {
        NSDictionary * redditJson = [NSJSONSerialization JSONObjectWithData:thePosts options:0 error:nil];
        for (NSInteger i=0; i<[redditJson[@"data"][@"children"] count]; i++){
            RedditPosts * getNew = [self addTheNewPosts:redditJson[@"data"][@"children"][i]];
            if (getNew !=nil) {
                [newPosts addObject:getNew];
            }
        }
        
    }
}
// add info to RedditPosts object

-(RedditPosts *)addTheNewPosts:(NSDictionary *)theNewStuff{
    RedditPosts * posts = [[RedditPosts alloc]init];
    posts.theTitle = (theNewStuff[@"data"][@"title"]);
    posts.theLink = (theNewStuff[@"data"][@"url"]);
    posts.theAuthor = (theNewStuff[@"data"][@"author"]);
    posts.theSource = (theNewStuff[@"data"][@"domain"]);
    return posts;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return newPosts.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    RedditPosts * theposts = [newPosts objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    
    //Table Cell
    
    cell.textLabel.text = theposts.theTitle;
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont fontWithName:@"OratorStd" size:13];
    
    cell.detailTextLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.font = [UIFont fontWithName:@"OratorStd" size:10];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Posted By: %@",theposts.theAuthor];
    
    cell.selectedBackgroundView = [UIView new];
    cell.selectedBackgroundView.layer.cornerRadius = 10;
    cell.selectedBackgroundView.alpha = 0.5;
    cell.selectedBackgroundView.layer.masksToBounds = YES;
    cell.selectedBackgroundView.backgroundColor = [UIColor colorWithRed:0 green:0.659 blue:0.91 alpha:0.4]; //#00a8e8
    return cell;
    

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SKAction * clickedSound = [SKAction playSoundFileNamed:@"click2.wav" waitForCompletion:NO];
    if ([GameScene getAudioPOS] == 0) {
        [theSceneR runAction:clickedSound];
    }
    doneButtonR.hidden = NO;
    redditTableView.hidden = YES;
    descTextR.hidden = YES;
    
    RedditPosts * thePost = [newPosts objectAtIndex:indexPath.row];
    //Map URL & Request
    NSString * linkURLstring = thePost.theLink;
    NSURL * URL = [NSURL URLWithString:linkURLstring];
    theUrlRequest = [NSURLRequest requestWithURL:URL cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30];
    
    
    [theSceneR.view addSubview:webViewR];
    [webViewR setClipsToBounds:YES];
    [webViewR loadRequest:theUrlRequest];
    
}
+(void)doneTouched{
    webViewR.hidden = YES;
    scanLineR.hidden = NO;
    doneButtonR.hidden = YES;
    redditTableView.hidden = NO;
    loadingIndicatorR.hidden = YES;
    descTextR.hidden = NO;
    [webViewR removeFromSuperview];
    theUrlRequest = [NSURLRequest requestWithURL:noURL cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:1];
    [webViewR loadRequest:theUrlRequest];
    [scanLineR runAction:runScanSeqR completion:^{
        scanLineR.hidden = TRUE;
    }];
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
    webViewR.hidden = YES;
    scanLineR.hidden = NO;
    loadingIndicatorR.hidden = NO;
    [theSceneR.view addSubview:loadingIndicatorR];
    [loadingIndicatorR startAnimating];
    [scanLineR runAction:runScanSeqR completion:^{
        scanLineR.hidden = TRUE;
    }];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    webViewR.hidden = NO;
    loadingIndicatorR.hidden = YES;
    [loadingIndicatorR stopAnimating];
    [loadingIndicatorR removeFromSuperview];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}
@end
