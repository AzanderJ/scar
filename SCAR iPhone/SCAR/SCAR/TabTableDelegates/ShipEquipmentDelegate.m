//
//  ShipEquipmentDelegate.m
//  SCAR
//
//  Created by Alexzander Jaggi on 4/10/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import "ShipEquipmentDelegate.h"

NSDictionary * weapons;
NSMutableArray * fixWeapons;
NSMutableArray * gimbalWeapons;
NSDictionary * blankWeapons;
NSArray * weaponsHeaders;
NSArray * weaponKeys;
NSArray * availWeaponsList;
NSArray * fixedWepList;
NSArray * gimbalWepList;
NSArray * availHeaders;
NSIndexPath * theSelectedCellW;
NSUserDefaults * shipSavedW;
NSString * savedWeaponKey;
SKScene * theMainSceneW;

Ships * shipW;

@implementation ShipEquipmentDelegate

+(ShipEquipmentDelegate *)initShipMountDelegateWithShip:(Ships *)theShip theWeapons:(NSArray *)theWeapons theFixedWep:(NSArray *)fixedArray theGimbalArray:(NSArray *)gimbalArray theAvalibleDefense:(NSArray *)theAvalibleDefense theScene:(SKScene *)theScene{
    static ShipEquipmentDelegate * shipDelegate;
    availWeaponsList = theWeapons;
    fixedWepList = fixedArray;
    gimbalWepList = gimbalArray;
    shipW = theShip;
    savedWeaponKey = [NSString stringWithFormat:@"%@Weapons",shipW.shipModel];
    shipSavedW = [NSUserDefaults standardUserDefaults];
    weaponsHeaders = @[@"Fixed Mounts", @"Gimbal Mounts"];
    weaponKeys = @[@"fixed", @"gimbal"];
    fixWeapons = [[NSMutableArray alloc]init];
    gimbalWeapons = [[NSMutableArray alloc]init];
    theMainSceneW = theScene;
    
    availHeaders = @[@"Avalible Weapons"];
    
    blankWeapons = [self loadBlanks:0];
    weapons = blankWeapons;
    [WeaponTab setDescText];

    
    @synchronized (self) {
        if (!shipDelegate) {
            shipDelegate = [[self alloc]init];
        }
    }
    return shipDelegate;
}

+(NSDictionary *)loadBlanks:(int)weaponsOrDefense{
    NSDictionary * blanks;
    switch (weaponsOrDefense) {
        case 0:
            //blanks
            for (int i =0; i < [shipW.shipFixedAmount integerValue]; i++) {
                Weapons * blankWeapon = [[Weapons alloc]init];
                blankWeapon.wepName = @"Fixed Empty";
                blankWeapon.wepSize = shipW.shipFixedSize;
                [fixWeapons addObject:blankWeapon];
            }
            for (int i = 0; i < [shipW.shipGimbaledAmount integerValue]; i++) {
                Weapons * blankWeapon = [[Weapons alloc]init];
                blankWeapon.wepName = @"Gimbal Empty";
                blankWeapon.wepSize = shipW.shipGimbalSize;
                [gimbalWeapons addObject:blankWeapon];
            }
            blanks = @{@"fixed": fixWeapons, @"gimbal":gimbalWeapons};
            break;

        default:
            break;
    }
    return blanks;
}

//tableview for equipment view tabs
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    long theSections = 0;
    
    switch (tableView.tag) {
        case 0:
            theSections = weaponsHeaders.count;
            break;
        
        case 2:
            theSections = availHeaders.count;
        default:
            break;

    }
    return theSections;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    switch (tableView.tag) {
        case 0:
            return [weaponsHeaders objectAtIndex:section];
            break;
        
        case 2:
            return [availHeaders objectAtIndex:section];
            break;
        default:
            break;
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    long theRows = 0;
    NSArray * theWeaps;
    
    switch (tableView.tag) {
        case 0:
            theWeaps = [weapons objectForKey:[weaponKeys objectAtIndex:section]];
            theRows = theWeaps.count;
            break;
        
        case 2:
            theRows = availWeaponsList.count;
            break;
        
        default:
            break;
    }
    
    return theRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString * theArrayKeys;
    NSArray * theItems;
    Weapons * weaps;
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    //Table Cell
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont fontWithName:@"OratorStd" size:10];
    cell.textLabel.alpha = 0.8;
    cell.detailTextLabel.alpha = 0.8;
    cell.detailTextLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.font = [UIFont fontWithName:@"OratorStd" size:10];
    cell.selectedBackgroundView = [UIView new];
    cell.selectedBackgroundView.layer.cornerRadius = 10;
    cell.selectedBackgroundView.alpha = 0.5;
    cell.selectedBackgroundView.layer.masksToBounds = YES;
    cell.selectedBackgroundView.backgroundColor = [UIColor colorWithRed:0 green:0.659 blue:0.91 alpha:0.4]; //#00a8e8
    cell.imageView.alpha = 0.8;
    
    
    switch (tableView.tag) {
        case 0:
            theArrayKeys = [weaponKeys objectAtIndex:indexPath.section];
            theItems = [weapons objectForKey:theArrayKeys];
            weaps = [theItems objectAtIndex:indexPath.row];
            cell.textLabel.text = weaps.wepName;
            cell.detailTextLabel.text = [NSString stringWithFormat:@"DPS: %@",weaps.wepDPS];
            cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"size%@",weaps.wepSize]];
            break;
        case 2:
            weaps = [availWeaponsList objectAtIndex:indexPath.row];
            cell.textLabel.text = weaps.wepName;
            cell.detailTextLabel.text = [NSString stringWithFormat:@"DPS: %@",weaps.wepDPS];
             cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"size%@",weaps.wepSize]];
            break;
        
            
        default:
            break;
    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    
    view.tintColor = [UIColor colorWithRed:0 green:0.494 blue:0.655 alpha:0.4]; //#007ea7
    view.layer.cornerRadius = 10;
    view.layer.masksToBounds = YES;
    
    UITableViewHeaderFooterView * header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setFont:[UIFont fontWithName:@"OratorStd" size:10]];
    [header.textLabel setTextColor:[UIColor colorWithRed:0 green:0.204 blue:0.349 alpha:1]]; //#003459
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Weapons * selectedWeapon;
    SKAction * clickedSound = [SKAction playSoundFileNamed:@"click2.wav" waitForCompletion:NO];
    SKAction * addedSound = [SKAction playSoundFileNamed:@"click3.wav" waitForCompletion:NO];


    switch (tableView.tag) {
        case 0:
            theSelectedCellW = indexPath;
            switch (indexPath.section) {
                case 0: //fixed weapon selected
                    availWeaponsList = fixedWepList;
                    if ([GameScene getAudioPOS] == 0) {
                        [theMainSceneW runAction:clickedSound];
                    }
                    [WeaponTab reloadAvaliWeaponTable];
                    break;
                case 1: //gimbal weapon selected
                    availWeaponsList = gimbalWepList;
                    if ([GameScene getAudioPOS] == 0) {
                        [theMainSceneW runAction:clickedSound];
                    }
                    [WeaponTab reloadAvaliWeaponTable];
                    break;
                default:
                    break;
            }
            break;

        case 2:
            switch ([theSelectedCellW section]) {
                case 0://Add weapon to fixed mount
                    selectedWeapon = [fixedWepList objectAtIndex:indexPath.row];
                    [[weapons objectForKey:@"fixed"]replaceObjectAtIndex:[theSelectedCellW row] withObject:selectedWeapon];
                    [WeaponTab reloadShipMountsTable:indexPath selectPath:theSelectedCellW];
                    [WeaponTab activateSaveButton];
                    [WeaponTab activateSimButton];
                    [WeaponTab setDescText];
                    if ([GameScene getAudioPOS] == 0) {
                        [theMainSceneW runAction:addedSound];
                    }
                    if ([shipSavedW rm_customObjectForKey:savedWeaponKey] != NULL) {
                        [WeaponTab activateLoadButton];
                    }
                    break;
                case 1://add weapon to gimbal mount
                    selectedWeapon = [gimbalWepList objectAtIndex:indexPath.row];
                    [[weapons objectForKey:@"gimbal"]replaceObjectAtIndex:[theSelectedCellW row] withObject:selectedWeapon];
                    [WeaponTab reloadShipMountsTable:indexPath selectPath:theSelectedCellW];
                    [WeaponTab activateSaveButton];
                    [WeaponTab activateSimButton];
                    [WeaponTab setDescText];
                    if ([GameScene getAudioPOS] == 0) {
                        [theMainSceneW runAction:addedSound];
                    }
                    if ([shipSavedW rm_customObjectForKey:savedWeaponKey] != NULL) {
                        [WeaponTab activateLoadButton];
                    }
                    break;
                default:
                    break;
            }
            
            break;
        default:
            break;
    }
}

+(void)savedClicked{
    [shipSavedW rm_setCustomObject:weapons forKey:savedWeaponKey];
    [WeaponTab activateDeleteButton];
    [WeaponTab deactivateLoadButton];
    [WeaponTab deactivateSaveButton];
}
+(void)loadClicked{
    weapons = [shipSavedW rm_customObjectForKey:savedWeaponKey];
    [WeaponTab reloadShipMountsTable:0 selectPath:0];
    [WeaponTab activateDeleteButton];
    [WeaponTab activateSimButton];
    [WeaponTab deactivateLoadButton];
    [WeaponTab deactivateSaveButton];
    [WeaponTab setDescText];
}
+(void)deleteClicked{
    weapons = [NSDictionary dictionaryWithDictionary:blankWeapons];
    [shipSavedW removeObjectForKey:savedWeaponKey];
    [WeaponTab reloadShipMountsTable:0 selectPath:0];
    [WeaponTab deactivateDeleteButton];
    [WeaponTab deactivateLoadButton];
    [WeaponTab deactivateSaveButton];
    [WeaponTab deactivateSimButton];
    [WeaponTab setDescText];
}
+(void)simClicked{
    [EquipmentView hideView:YES];
    [WeaponTab hideView:YES];
    [TopMenu hideView:YES];
    [GameScene hideView:YES];
    [GameScene activateSim:shipW whatSim:0 playerDPS:[[self getDPS]intValue] playerShield:0];
}
+(void)stopSim{
    [EquipmentView hideView:NO];
    [WeaponTab hideView:NO];
    [TopMenu hideView:NO];
    [GameScene hideView:NO];
}

+(NSNumber *)getDPS{
    NSArray * fixedDPS = [weapons objectForKey:@"fixed"];
    NSArray * gimbalDPS = [weapons objectForKey:@"gimbal"];
    NSMutableArray * allDPS = [[NSMutableArray alloc]init];
    Weapons * weaponDPS;
    
    for (int i = 0; i <[fixedDPS count]; ++i ) {
        weaponDPS = [fixedDPS objectAtIndex:i];
        NSString * fixDps = weaponDPS.wepDPS;
        if (fixDps != NULL) {
            [allDPS addObject:fixDps];
        }
    }
    for (int i = 0; i <[gimbalDPS count]; ++i)  {
        weaponDPS = [gimbalDPS objectAtIndex:i];
        NSString * gimDps = weaponDPS.wepDPS;
        if (gimDps != NULL) {
            [allDPS addObject:gimDps];
        }
    }
    NSNumber * theSum = [allDPS valueForKeyPath:@"@sum.floatValue"];
    return theSum;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}
@end
