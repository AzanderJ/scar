//
//  DefenseDelegate.m
//  SCAR
//
//  Created by Alexzander Jaggi on 4/14/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import "DefenseDelegate.h"

NSDictionary * defense;
NSDictionary * blankDef;
NSMutableArray * defenseStuff;
NSArray * defenseHeaders;
NSArray * defenseKeys;
NSArray * defenseEquip;
NSArray * defHeaders;
NSIndexPath * theSelectedCell;
NSUserDefaults * shipSaved;
int shieldHP;
NSString * savedDefenseKey;
SKScene * theSceneDD;

Ships * ship;

@implementation DefenseDelegate
+(DefenseDelegate *)initDefenseDelegateWithShip:(Ships *)theShip theAvalibleDefense:(NSArray *)theAvalibleDefense theScene:(SKScene *)theScene{
    static DefenseDelegate * defenseDelegate;
    ship = theShip;
    defenseEquip = theAvalibleDefense;
    savedDefenseKey = [NSString stringWithFormat:@"%@Defense",theShip.shipModel];
    shipSaved = [NSUserDefaults standardUserDefaults];
    theSceneDD = theScene;
    
    defenseHeaders = @[@"Shield"];
    defenseKeys = @[@"shield"];
    defenseStuff = [[NSMutableArray alloc]init];
    
    defHeaders = @[@"Avalible Defense"];
    
    blankDef = [self loadBlanks:1];
    defense = blankDef;

    
    @synchronized (self) {
        if (!defenseDelegate) {
            defenseDelegate = [[self alloc]init];
        }
    }
    return defenseDelegate;
}

+(NSDictionary *)loadBlanks:(int)weaponsOrDefense{
    NSDictionary * blanks;
    DefenseEquipment * blankDef;
    switch (weaponsOrDefense) {

        case 1:
            blankDef = [[DefenseEquipment alloc]init];
            blankDef.defenseName = @"Empty";
            blankDef.defenseSize = ship.shipShield;
            [defenseStuff addObject:blankDef];
            blanks = @{@"shield":defenseStuff};
            break;
        default:
            break;
    }
    return blanks;
}

//tableview for equipment view tabs
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    long theSections = 0;
    
    switch (tableView.tag) {
        case 0:

        case 1:
            theSections = defenseHeaders.count;
            break;

        default:
            break;
        case 3:
            theSections = defHeaders.count;
            break;
    }
    return theSections;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    switch (tableView.tag) {

        case 1:
            return [defenseHeaders objectAtIndex:section];
            break;

        case 3:
            return [defHeaders objectAtIndex:section];
            break;
        default:
            break;
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    long theRows = 0;
    NSArray *theDef;
    
    switch (tableView.tag) {

        case 1:
            theDef = [defense objectForKey:[defenseKeys objectAtIndex:section]];
            theRows = theDef.count;
            break;

        case 3:
            theRows = defenseEquip.count;
            break;
        default:
            break;
    }
    
    return theRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString * theArrayKeys;
    NSArray * theItems;
    DefenseEquipment * def;
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    //Table Cell
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont fontWithName:@"OratorStd" size:10];
    cell.textLabel.alpha = 0.8;
    cell.detailTextLabel.alpha = 0.8;
    cell.detailTextLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.font = [UIFont fontWithName:@"OratorStd" size:10];
    cell.selectedBackgroundView = [UIView new];
    cell.selectedBackgroundView.layer.cornerRadius = 10;
    cell.selectedBackgroundView.alpha = 0.5;
    cell.selectedBackgroundView.layer.masksToBounds = YES;
    cell.selectedBackgroundView.backgroundColor = [UIColor colorWithRed:0 green:0.659 blue:0.91 alpha:0.4]; //#00a8e8
    cell.imageView.alpha = 0.8;
    
    switch (tableView.tag) {

        case 1:
            theArrayKeys = [defenseKeys objectAtIndex:indexPath.section];
            theItems = [defense objectForKey:theArrayKeys];
            def = [theItems objectAtIndex:indexPath.row];
            cell.textLabel.text = def.defenseName;
            cell.detailTextLabel.text = [NSString stringWithFormat:@"Hit Points: %@",def.defenseHP];
            cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"size%@",def.defenseSize]];
            break;

        case 3:
            def = [defenseEquip objectAtIndex:indexPath.row];
            cell.textLabel.text = def.defenseName;
            cell.detailTextLabel.text = [NSString stringWithFormat:@"Hit Points: %@",def.defenseHP];
            cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"size%@",def.defenseSize]];
            break;
            
        default:
            break;
    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    
    view.tintColor = [UIColor colorWithRed:0 green:0.494 blue:0.655 alpha:0.4]; //#007ea7
    view.layer.cornerRadius = 10;
    view.layer.masksToBounds = YES;
    
    UITableViewHeaderFooterView * header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setFont:[UIFont fontWithName:@"OratorStd" size:10]];
    [header.textLabel setTextColor:[UIColor colorWithRed:0 green:0.204 blue:0.349 alpha:1]]; //#003459
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DefenseEquipment * selectedDefense;
    SKAction * clickedSound = [SKAction playSoundFileNamed:@"click2.wav" waitForCompletion:NO];
    SKAction * addedSound = [SKAction playSoundFileNamed:@"click3.wav" waitForCompletion:NO];

    switch (tableView.tag) {

        case 1:
            theSelectedCell = indexPath;
            if ([GameScene getAudioPOS] == 0) {
                [theSceneDD runAction:clickedSound];
            }
            
            break;

        case 3:
            selectedDefense = [defenseEquip objectAtIndex:indexPath.row];
            shieldHP = [selectedDefense.defenseHP intValue] ;
            [[defense objectForKey:@"shield"]replaceObjectAtIndex:[theSelectedCell row] withObject:selectedDefense];
            [DefenseTab reloadDefenseShipMounts:indexPath selectPath:theSelectedCell];
            [DefenseTab activateSaveButton];
            [DefenseTab activateSimButton];
            
            if ([GameScene getAudioPOS] == 0) {
                [theSceneDD runAction:addedSound];
            }
            if ([shipSaved rm_customObjectForKey:savedDefenseKey] != NULL) {
                [DefenseTab activateLoadButton];
            }
            break;
        default:
            break;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}

+(void)savedClicked{
    [shipSaved rm_setCustomObject:defense forKey:savedDefenseKey];
    [DefenseTab activateDeleteButton];
    [DefenseTab deactivateSaveButton];
    [DefenseTab deactivateLoadButton];
}
+(void)loadClicked{
    defense = [shipSaved rm_customObjectForKey:savedDefenseKey];
    [DefenseTab reloadDefenseShipMounts:0 selectPath:0];
    [DefenseTab activateDeleteButton];
    [DefenseTab activateSimButton];
    [DefenseTab deactivateLoadButton];
    [DefenseTab deactivateSaveButton];
}
+(void)deleteClicked{
    defense = blankDef;
    [shipSaved removeObjectForKey:savedDefenseKey];
    [DefenseTab reloadDefenseShipMounts:0 selectPath:0];
    [DefenseTab deactivateDeleteButton];
    [DefenseTab deactivateLoadButton];
    [DefenseTab deactivateSaveButton];
    [DefenseTab deactivateSimButton];
}
+(void)simClicked{
    [EquipmentView hideView:YES];
    [TopMenu hideView:YES];
    [DefenseTab hideView:YES];
    [GameScene hideView:YES];
    
    [GameScene activateSim:ship whatSim:1 playerDPS:0 playerShield:shieldHP];
}
+(void)stopSim{
    [EquipmentView hideView:NO];
    [DefenseTab hideView:NO];
    [TopMenu hideView:NO];
    [GameScene hideView:NO];
}

@end
