//
//  FirebaseData.h
//  SCAR
//
//  Created by Alexzander Jaggi on 4/3/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Firebase/Firebase.h>

#import "Ships.h"
#import "Weapons.h"
#import "DefenseEquipment.h"
#import "SystemEquipment.h"

@interface FirebaseData : NSObject

+(NSArray *)getShipList;
+(NSArray *)getShipWeaponsFixedMaxSize:(NSString *)fixedMaxSize gimbalMaxSize:(NSString *)gimbalMaxSize;
+(NSMutableArray *)getShipWeaponsSingleType:(NSString *)theSize;
+(NSArray *)getDefenseEquipmentBySize:(NSString *)defenseSize;
+(NSDictionary *)getSystemEquipment;

@end
