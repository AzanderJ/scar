//
//  EnemyShip.m
//  SCAR
//
//  Created by Alexzander Jaggi on 4/17/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import "EnemyShip.h"

@implementation EnemyShip
EnemyShip * enemyShip;

+(EnemyShip *) initNewEnemyShip:(SKScene*)theScene{
    
    enemyShip = [EnemyShip spriteNodeWithImageNamed:@"glaiveR"];
    enemyShip.position = CGPointMake(theScene.size.width/1.3, theScene.size.height/2);
    enemyShip.zPosition = 3;
    enemyShip.yScale = 0.8;
    enemyShip.xScale = 0.8;
    enemyShip.name = @"enemyShip";
    enemyShip.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:enemyShip.frame.size];
    enemyShip.physicsBody.allowsRotation = FALSE;
    enemyShip.physicsBody.mass = 20000;
    [theScene addChild:enemyShip];
    
    return enemyShip;
}
@end
