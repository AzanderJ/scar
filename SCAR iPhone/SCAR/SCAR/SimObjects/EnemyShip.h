//
//  EnemyShip.h
//  SCAR
//
//  Created by Alexzander Jaggi on 4/17/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface EnemyShip : SKSpriteNode

+(EnemyShip *) initNewEnemyShip:(SKScene *)theScene;

@end
