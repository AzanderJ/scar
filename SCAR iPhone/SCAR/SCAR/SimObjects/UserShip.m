//
//  UserShip.m
//  SCAR
//
//  Created by Alexzander Jaggi on 4/17/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import "UserShip.h"

@implementation UserShip

UserShip * userShipSprite;

+(UserShip *) initNewUserShip:(SKScene*)theScene theShip:(Ships *)theShip{
    userShipSprite = [UserShip spriteNodeWithImageNamed:theShip.shipIMG];
    userShipSprite.position = CGPointMake(theScene.size.width/4, theScene.size.height/2);
    userShipSprite.zPosition = 3;
    userShipSprite.yScale = 0.8;
    userShipSprite.xScale = 0.8;
    userShipSprite.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:userShipSprite.frame.size];
    userShipSprite.physicsBody.allowsRotation = FALSE;
    userShipSprite.name = @"userShip";
    userShipSprite.physicsBody.mass = 20000;
    
    [theScene addChild: userShipSprite];
    
    return userShipSprite;
}
@end
