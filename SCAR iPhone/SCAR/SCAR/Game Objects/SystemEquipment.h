//
//  SystemEquipment.h
//  SCAR
//
//  Created by Alexzander Jaggi on 4/9/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SystemEquipment : NSObject


@property (nonatomic) NSString * itemName;
@property (nonatomic) NSString * itemAblility;
@property (nonatomic) NSString * itemDescription;

@end