//
//  Credits.h
//  SCAR
//
//  Created by Alexzander Jaggi on 4/24/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Settings.h"

@interface Credits : SKLabelNode
+(Credits *)intLabel:(SKScene *)scene labelText:(int)text;

@end
