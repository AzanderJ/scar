//
//  RedditPosts.h
//  SCAR
//
//  Created by Alexzander Jaggi on 4/12/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RedditPosts : NSObject
@property (nonatomic) NSString * theAuthor;
@property (nonatomic) NSString * theTitle;
@property (nonatomic) NSNumber * theTime;
@property (nonatomic) NSString * theSource;
@property (nonatomic) NSString * theLink;
@property (nonatomic) NSString * theSubreddit;
@end
