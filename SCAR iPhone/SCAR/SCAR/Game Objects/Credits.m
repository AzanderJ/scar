//
//  Credits.m
//  SCAR
//
//  Created by Alexzander Jaggi on 4/24/16.
//  Copyright © 2016 Alexzander Jaggi. All rights reserved.
//

#import "Credits.h"

@implementation Credits
Credits * creditLabel;
SKSpriteNode * cBG;

+(Credits *)intLabel:(SKScene *)scene labelText:(int)text{
    NSArray * credits = [NSArray arrayWithObjects:@"Alexzander Jaggi - Developer",@"Space Ship Icon By: Sergey Demushkin",@"Star Citizen Pixel Ships By: Haarlinsh" , @"Reddit Logo By: Neiio", @"SCAR Logo By: Prometheus Naiima",@"Load Weapon Sound By: Pera",@"Menu Select Sound By: Fins", @"2D Map By: Selbie Le-Grille", @"Weapon Symbols By: Icons8",  nil];
    cBG = [Settings getCreditsBG];
    
    
    if (text<credits.count) {
        creditLabel = [Credits labelNodeWithFontNamed:@"OratorStd"];
        creditLabel.fontSize = 18;
        creditLabel.position = CGPointMake(0, -scene.size.height/2);
        creditLabel.text = [credits objectAtIndex:text];
        creditLabel.name = @"creditsLabel";
        creditLabel.zPosition = 21;
        
        SKAction * scroll = [SKAction moveTo:CGPointMake(0, scene.size.height + creditLabel.frame.size.height) duration:10 ];
        
        [creditLabel runAction:scroll];
        [cBG addChild:creditLabel];
        return creditLabel;
    }
    
    return nil;
}
@end
