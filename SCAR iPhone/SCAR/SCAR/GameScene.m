//
//  GameScene.m
//  SCAR
//
//  Created by Alexzander Jaggi on 3/22/16.
//  Copyright (c) 2016 Alexzander Jaggi. All rights reserved.
//

#import "GameScene.h"

static const uint32_t userShipCategory = 0x1;
static const uint32_t enemyShipCategory = 0x1 << 1;
static const uint32_t weaponCategory = 0x1 << 2;

@implementation GameScene

double asteroidTimer;
double weaponsTimer;
double nextCredit;
int creditIndex;
int audioPOS;
int equipOpen = 0;
int mapOpen = 0;
int redditOpen = 0;
int settingsOpen = 0;
int shipSelectorOpen = 0;
int tab1 = 1;
int tab2 = 0;
int tab3 = 0;
int tab4 = 0;
int info = 0;
int enemyDPS;
int enemySheild;
int thePlayerDPS;
int thePlayerSheild;
int theSimType;
int fireTime;
int fireSec;
SKSpriteNode * selectionIndicator1;
SKSpriteNode * selectionIndicator2;
SKSpriteNode * selectionIndicator3;
SKSpriteNode * equipmentIcon;
SKSpriteNode * mapIcon;
SKSpriteNode * redditIcon;
SKSpriteNode * sideDock;
SKSpriteNode * background;
SKSpriteNode * stopButton;
SKSpriteNode * shieldNode;
SKSpriteNode * gearButton;
SKLabelNode * timeToKillLabel;
NSArray * shieldFrames;
SKScene * thisSceneSim;
UserShip * userShipSim;
EnemyShip * enemyShipSim;
NSUserDefaults* defaults;
WeaponsFire * theFire;
NSDictionary * startupAudio;



-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    self.physicsWorld.gravity = CGVectorMake(0, 0);
    self.physicsWorld.contactDelegate = self;
    enemyDPS = 2250;
    enemySheild = 1800;
    thisSceneSim = self;
    defaults = [NSUserDefaults standardUserDefaults];
    [Firebase defaultConfig].persistenceEnabled = YES;
    if ([self isConnected] == false) {
        if (![defaults rm_customObjectForKey:@"startup"]) {
            [self showAlert:@"SCAR Requires A Network Connection For It's First Run"];
        }
    }
    
    if (![defaults rm_customObjectForKey:@"startup"]) {
        startupAudio = @{@"audio":@0};
        [defaults rm_setCustomObject:startupAudio forKey:@"startup"];
    }else{
        startupAudio = [defaults rm_customObjectForKey:@"startup"];
        audioPOS = [[startupAudio objectForKey:@"audio"]intValue];
    }
    
    [self constructUI];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    SKAction * clickedSound = [SKAction playSoundFileNamed:@"click.wav" waitForCompletion:NO];
    UITouch * touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKSpriteNode * theNode = (SKSpriteNode *)[self nodeAtPoint:location];
    
    //Sidebar buttons
    if ([[theNode name]isEqualToString:@"equipment"]) {
        if (audioPOS == 0) {
            [self runAction:clickedSound];

        }
        switch (equipOpen) {
            case 0:
                [EquipmentView openEquipmentView:self];
                equipOpen = 1;
                [equipmentIcon addChild:selectionIndicator1];
                break;
            case 1:
                [EquipmentView closeEquipmentView];
                [selectionIndicator1 removeFromParent];
                equipOpen = 0;
                tab1 = 0;
                tab2 = 0;
                tab3 = 0;
                tab4 = 0;
                break;
            default:
                break;
        }
        switch (mapOpen) {
            case 1:
                [MapView closeMapView];
                [selectionIndicator2 removeFromParent];
                mapOpen = 0;
                break;
            default:
                break;
        }
        switch (redditOpen) {
            case 1:
                [RedditView closeRedditView];
                [selectionIndicator3 removeFromParent];
                redditOpen = 0;
                break;
            default:
                break;
        }

    }
    if ([[theNode name]isEqualToString:@"map"]){
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        if ([self isConnected]!=TRUE) {
            [self showAlert:[NSString stringWithFormat:@"The 3D Map Requires an Active Internet Connection"]];

        }
        switch (mapOpen) {
            case 0:
                [MapView openMapView:self];
                [mapIcon addChild:selectionIndicator2];
                mapOpen = 1;
                break;
            case 1:
                [MapView closeMapView];
                [selectionIndicator2 removeFromParent];
                mapOpen = 0;
                break;
            default:
                break;
        }
        switch (equipOpen) {
            case 1:
                [EquipmentView closeEquipmentView];
                [selectionIndicator1 removeFromParent];
                equipOpen = 0;
                tab1 = 0;
                tab2 = 0;
                tab3 = 0;
                tab4 = 0;
                break;
                
            default:
                break;
        }
        switch (redditOpen) {
            case 1:
                [RedditView closeRedditView];
                [selectionIndicator3 removeFromParent];
                redditOpen = 0;
                break;
            default:
                break;
        }
    }
    if ([[theNode name]isEqualToString:@"reddit"]) {
        if ([self isConnected]!=TRUE) {
            [self showAlert:[NSString stringWithFormat:@"This View Requires an Active Internet Connection"]];
            
        }
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        switch (redditOpen) {
            case 0:
                [RedditView openRedditView:self];
                [redditIcon addChild:selectionIndicator3];
                redditOpen = 1;
                break;
            case 1:
                [RedditView closeRedditView];
                [selectionIndicator3 removeFromParent];
                redditOpen = 0;
                break;
            default:
                break;
        }
        switch (equipOpen) {
            case 1:
                [EquipmentView closeEquipmentView];
                [selectionIndicator1 removeFromParent];
                equipOpen = 0;
                tab1 = 0;
                tab2 = 0;
                tab3 = 0;
                tab4 = 0;
                break;
                
            default:
                break;
        }
        switch (mapOpen) {
            case 1:
                [MapView closeMapView];
                [selectionIndicator2 removeFromParent];
                mapOpen = 0;
                break;
            default:
                break;
        }

    }
    
    //Equipment view tabs
    if ([[theNode name]isEqualToString:@"generalTab"]){ //tab1 General
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        switch (tab1) {
            case 0:
                tab1 = 1;
                [EquipmentView activateGeneralTab];
                break;
        }
        switch (tab2) {
            case 1:
                tab2 = 0;
                [EquipmentView deactivateWeaponTab];
                break;
            default:
                break;
        }
        switch (tab3) {
            case 1:
                tab3 = 0;
                [EquipmentView deactivateDefenseTab];
                break;
            default:
                break;
        }
        switch (tab4) {
            case 1:
                tab4 = 0;
                [EquipmentView deactivateSystemTab];
                break;
            default:
                break;
        }
    }
    if ([[theNode name]isEqualToString:@"weaponsTab"]) { //tab2 Weapons
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        switch (tab1) {
            case 1:
                tab1 = 0;
                [EquipmentView deactivateGeneralTab];
                break;
        }
        switch (tab2) {
            case 0:
                tab2 = 1;
                [EquipmentView activateWeaponTab];
                break;
            default:
                break;
        }
        switch (tab3) {
            case 1:
                tab3 = 0;
                [EquipmentView deactivateDefenseTab];
                break;
            default:
                break;
        }
        switch (tab4) {
            case 1:
                tab4 = 0;
                [EquipmentView deactivateSystemTab];
                break;
            default:
                break;
        }
    }
    if ([[theNode name]isEqualToString:@"defenseTab"]) { //tab3 Defense
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        switch (tab1) {
            case 1:
                tab1 = 0;
                [EquipmentView deactivateGeneralTab];
                break;
        }
        switch (tab2) {
            case 1:
                tab2 = 0;
                [EquipmentView deactivateWeaponTab];
                break;
            default:
                break;
        }
        switch (tab3) {
            case 0:
                tab3 = 1;
                [EquipmentView activateDefenseTab];
                break;
            default:
                break;
        }
        switch (tab4) {
            case 1:
                tab4 = 0;
                [EquipmentView deactivateSystemTab];
                break;
            default:
                break;
        }
    }
    if ([[theNode name]isEqualToString:@"systemTab"]) { //tab4 Systems
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        switch (tab1) {
            case 1:
                tab1 = 0;
                [EquipmentView deactivateGeneralTab];
                break;
        }
        switch (tab2) {
            case 1:
                tab2 = 0;
                [EquipmentView deactivateWeaponTab];
                break;
            default:
                break;
        }
        switch (tab3) {
            case 1:
                tab3 = 0;
                [EquipmentView deactivateDefenseTab];
                break;
            default:
                break;
        }
        switch (tab4) {
            case 0:
                tab4 = 1;
                [EquipmentView activateSystemTab];
                break;
            default:
                break;
        }

    }
    
    // equipment view/top bar stuff
    if ([[theNode name]isEqualToString:@"shipButton"]) {
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        switch (shipSelectorOpen) {
            case 0:
                shipSelectorOpen = 1;
                [EquipmentView activeShipSelector];
                break;
            case 1:
                shipSelectorOpen = 0;
                [EquipmentView deactivateShipSelector];
                
            default:
                break;
        }
    }
    if ([[theNode name]isEqualToString:@"save"]) {
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        if (tab2 == 1) {
            [ShipEquipmentDelegate savedClicked];
        }
        if (tab3 == 1) {
            [DefenseDelegate savedClicked];
        }
        
    };
    if ([[theNode name]isEqualToString:@"load"]) {
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        if (tab2 == 1) {
            [ShipEquipmentDelegate loadClicked];

        }
        if (tab3 == 1) {
            [DefenseDelegate loadClicked];
        }
    }
    if ([[theNode name]isEqualToString:@"delete"]) {
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        if(tab2 == 1){
            [ShipEquipmentDelegate deleteClicked];
        }
        if (tab3 == 1) {
            [DefenseDelegate deleteClicked];
        }
    }
    if ([[theNode name]isEqualToString:@"sim"]) {
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        if (tab2 == 1) {
            [ShipEquipmentDelegate simClicked];
        }
        if (tab3 == 1) {
            [DefenseDelegate simClicked];
        }
    }
    if ([[theNode name]isEqualToString:@"stop"]) {
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        [userShipSim removeFromParent];
        [enemyShipSim removeFromParent];
        [stopButton removeFromParent];
        [timeToKillLabel removeFromParent];

        if (tab2 == 1) {
            [ShipEquipmentDelegate stopSim];
        }
        if (tab3 == 1) {
            [DefenseDelegate stopSim];
        }
    }
    
    //map tab stuff
    if ([[theNode name]isEqualToString:@"doneR"]) {
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        [RedditView doneTouched];
    }
    if ([[theNode name]isEqualToString:@"interactiveMap"]) {
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        [MapView activateMap:0];
    }
    if ([[theNode name]isEqualToString:@"flatMapToggle"]) {
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        [MapView activateMap:1];
    }
    if ([[theNode name]isEqualToString:@"infoButton"]) { // settings bar/showcredits
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        
        if (info == 0) {
            if (equipOpen == 1) {
                [EquipmentView closeEquipmentView];
                [selectionIndicator1 removeFromParent];
                equipOpen = 0;
                tab1 = 0;
                tab2 = 0;
                tab3 = 0;
                tab4 = 0;
            }
            if (redditOpen == 1) {
                [RedditView closeRedditView];
                [selectionIndicator3 removeFromParent];
                redditOpen = 0;
            }
            if (mapOpen == 1) {
                [MapView closeMapView];
                [selectionIndicator2 removeFromParent];
                mapOpen = 0;
            }
            background.zPosition = -4;
            [Settings addCreditsBG];
            [Settings removeSettings];
            settingsOpen = 0;
            info = 1;
        }
    }
    if ([[theNode name]isEqualToString:@"doneCredits"]) { //remove credits
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        background.zPosition = -2;
        [Settings removeCreditsBG];
        creditIndex = 0;
        info = 0;

    }
    if ([[theNode name]isEqualToString:@"settings"]) { //show settings bar
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        switch (settingsOpen) {
            case 0:
                [Settings initSettings:self];
                settingsOpen = 1;
                break;
            case 1:
                [Settings removeSettings];
                settingsOpen = 0;
                break;
            default:
                break;
        }
    }
    if ([[theNode name]isEqualToString:@"audio"]) { //settings bar/mute sounds
        if (audioPOS == 0) {
            [self runAction:clickedSound];
            
        }
        switch (audioPOS) {
            case 0:
                audioPOS = 1;
                [Settings setAudioPOS:1];
                startupAudio = @{@"audio":@1};
                [defaults rm_setCustomObject:startupAudio forKey:@"startup"];
                [Settings removeSettings];
                settingsOpen = 0;
                break;
            case 1:
                audioPOS = 0;
                [Settings setAudioPOS:0];
                startupAudio = @{@"audio":@0};
                [defaults rm_setCustomObject:startupAudio forKey:@"startup"];
                [Settings removeSettings];
                settingsOpen = 0;
                break;
                
            default:
                break;
        }
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    
    //Start Asteroids
    if (info == 0) {
        float randomSecs = [self randomValueBetween:3.0 andValue:5.0];
        if (currentTime > asteroidTimer) {
            asteroidTimer = randomSecs +currentTime;
            [Asteroids initAsteroids:self];
        }
    }
    if (info == 1) {
        double timeSet= 1;
        if (currentTime>nextCredit ) {
            [Credits intLabel:self labelText:creditIndex];
            nextCredit = currentTime + timeSet;
            ++creditIndex;
        }
    }

}

-(void)constructUI{
    
    //Scene background
    background = [SKSpriteNode spriteNodeWithImageNamed:@"spaceBackground"];
    background.position = CGPointMake(self.size.width/2, self.size.height/2);
    background.scene.scaleMode = SKSceneScaleModeAspectFill;
    background.zPosition = -2;
    
    //Side Dock
    sideDock = [SKSpriteNode spriteNodeWithImageNamed:@"sidedock"];
    sideDock.size = CGSizeMake(self.size.width/7, self.size.height/1.5);
    sideDock.position = CGPointMake(sideDock.frame.size.width/2, self.size.height/2);
    sideDock.zPosition = 0;
    
    //Dock Icons
    equipmentIcon = [SKSpriteNode spriteNodeWithImageNamed:@"shipIcon"];
    equipmentIcon.size = CGSizeMake(sideDock.size.width/1.5, sideDock.size.width/1.5);
    equipmentIcon.position = CGPointMake(sideDock.size.width/100-4 , sideDock.frame.size.height/3.5);
    equipmentIcon.name = @"equipment";
    equipmentIcon.zPosition = 2;
    equipmentIcon.alpha = 0.8;
    
    mapIcon = [SKSpriteNode spriteNodeWithImageNamed:@"map3"];
    mapIcon.size = CGSizeMake(sideDock.size.width/1.5, sideDock.size.width/1.5);
    mapIcon.position = CGPointMake(sideDock.size.width/100-4, sideDock.frame.size.height/65-5);
    mapIcon.name = @"map";
    mapIcon.zPosition = 2;
    
    redditIcon = [SKSpriteNode spriteNodeWithImageNamed:@"redditIcon"];
    redditIcon.size = CGSizeMake(sideDock.size.width/1.5, sideDock.size.width/1.6);
    redditIcon.position = CGPointMake(sideDock.size.width/100-4, sideDock.frame.size.height/2 - sideDock.size.height*0.8);
    redditIcon.name = @"reddit";
    redditIcon.zPosition = 2;
    
    selectionIndicator1 = [SKSpriteNode spriteNodeWithImageNamed:@"selected"];
    selectionIndicator1.zPosition = 1;
    selectionIndicator1.alpha = 0.35;
    selectionIndicator1.xScale = 0.5;
    selectionIndicator1.yScale = 0.5;
    
    selectionIndicator2 = [SKSpriteNode spriteNodeWithImageNamed:@"selected"];
    selectionIndicator2.zPosition = 1;
    selectionIndicator2.alpha = 0.35;
    selectionIndicator2.xScale = 0.5;
    selectionIndicator2.yScale = 0.5;
    
    selectionIndicator3 = [SKSpriteNode spriteNodeWithImageNamed:@"selected"];
    selectionIndicator3.zPosition = 1;
    selectionIndicator3.alpha = 0.35;
    selectionIndicator3.xScale = 0.5;
    selectionIndicator3.yScale = 0.5;
    
    gearButton = [SKSpriteNode spriteNodeWithImageNamed:@"gear"];
    gearButton.position = CGPointMake(thisSceneSim.size.width - 30, thisSceneSim.size.height - 20);
    gearButton.zPosition = 19;
    gearButton.yScale = 0.03;
    gearButton.xScale = 0.03;
    gearButton.alpha = 0.7;
    gearButton.name = @"settings";
    
    //--------------///-----------------//
    
    [self addChild:background];
    [self addChild:sideDock];
    [self addChild:gearButton];
    [sideDock addChild:equipmentIcon];
    [sideDock addChild:mapIcon];
    [sideDock addChild:redditIcon];
}

-(float)randomValueBetween:(float)low andValue:(float)high{
    return (((float)arc4random()/0xFFFFFFFFu)*(high-low))+low;
}



- (void)didBeginContact:(SKPhysicsContact *)contact{
    
    switch (theSimType) {
        case 0:
            if (contact.bodyB.categoryBitMask == weaponCategory) {
                [contact.bodyB.node removeFromParent];
                shieldNode.hidden = NO;
            }
            break;
        case 1:
            if (contact.bodyB.categoryBitMask == weaponCategory) {
                [contact.bodyB.node removeFromParent];
                shieldNode.hidden = NO;

            }
            
        default:
            break;
    }
    
}
- (void)didEndContact:(SKPhysicsContact *)contact{
    
}

+(void)activateSim:(Ships *)thePlayerShip whatSim:(int)simType playerDPS:(int)playerDPS playerShield:(int)playerShield{
    
    thePlayerDPS = playerDPS;
    thePlayerSheild = playerShield;
    
    userShipSim = [UserShip initNewUserShip:thisSceneSim theShip:thePlayerShip];
    userShipSim.physicsBody.categoryBitMask = userShipCategory;
    userShipSim.physicsBody.contactTestBitMask = weaponCategory;
    userShipSim.hidden = NO;
    
    enemyShipSim = [EnemyShip initNewEnemyShip:thisSceneSim];
    enemyShipSim.physicsBody.categoryBitMask = enemyShipCategory;
    enemyShipSim.physicsBody.contactTestBitMask = weaponCategory;
    enemyShipSim.hidden = NO;
    
    NSMutableArray * frames = [[NSMutableArray alloc]init];
    SKTextureAtlas * shieldAnimation = [ SKTextureAtlas atlasNamed:@"shield"];
    long numberIMG = shieldAnimation.textureNames.count;
    for (int i =1; i<=numberIMG; i++) {
        NSString *textureName = [NSString stringWithFormat:@"shield_0%d",i];
        SKTexture * temp = [shieldAnimation textureNamed:textureName];
        [frames addObject:temp];
    }
    shieldFrames = [[NSArray alloc]initWithArray: frames];
    SKTexture * temp = shieldFrames[0];
    shieldNode = [SKSpriteNode spriteNodeWithTexture:temp];
    shieldNode.hidden = YES;
    [self activateShield];
    stopButton = [SKSpriteNode spriteNodeWithImageNamed:@"doneButton"];
    stopButton.position = CGPointMake(thisSceneSim.size.width/2, stopButton.size.height);
    stopButton.name = @"stop";
    stopButton.xScale = 0.2;
    stopButton.yScale = 0.2;
    
    
    
    switch (simType) {
        case 0: //weapons sim
            
            
            shieldNode.size = CGSizeMake(enemyShipSim.size.width+shieldNode.size.width, enemyShipSim.size.height+shieldNode.size.height);
            shieldNode.position = CGPointMake(-enemyShipSim.frame.size.width/2+ shieldNode.frame.size.width/3.5, enemyShipSim.size.height/2 - shieldNode.frame.size.height/5);
            [enemyShipSim addChild:shieldNode];
            theSimType = 0;
            fireTime = [self getFireTime:playerDPS defense:enemySheild];
            fireSec = [self getFireTime:playerDPS defense:enemySheild];
            [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(launchWeapons:) userInfo:nil repeats:YES];

            break;
        case 1: //defense sim
            shieldNode.size = CGSizeMake(userShipSim.size.width+shieldNode.size.width, userShipSim.size.height+shieldNode.size.height);
            shieldNode.position = CGPointMake(-userShipSim.frame.size.width/2+ shieldNode.frame.size.width/3.5, userShipSim.size.height/2 - shieldNode.frame.size.height/5);
            [userShipSim addChild:shieldNode];
            
            theSimType = 1;
            fireTime = [self getFireTime:enemyDPS defense:playerShield];
            fireSec = [self getFireTime:enemyDPS defense:playerShield];
            [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(launchWeapons:) userInfo:nil repeats:YES];
            
            break;
        default:
            break;
    }
}

+(int)getFireTime:(int)dps defense:(int)defense{
    int fireTime = defense/dps;
    return fireTime;
}

+(void)activateShield{
    [shieldNode runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:shieldFrames timePerFrame:0.1f resize:NO restore:YES]]withKey:@"shieldOn"];
}

+(void)launchWeapons:(NSTimer *)timer{
    SKEmitterNode * weaponEffect = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle]pathForResource:@"weaponParticals" ofType:@"sks"]];

    if (fireTime >= 0) {
        fireTime = fireTime-1;
        
        if (theSimType == 0) {
            theFire = [WeaponsFire initFire:thisSceneSim startingPoint:CGPointMake(userShipSim.position.x + theFire.size.width+userShipSim.frame.size.width/2, userShipSim.position.y)];
            theFire.physicsBody.categoryBitMask = weaponCategory;
            theFire.physicsBody.contactTestBitMask = enemyShipCategory;
            [theFire addChild:weaponEffect];
            [WeaponsFire fireWeapon:0];
        }
        if (theSimType == 1) {
            theFire = [WeaponsFire initFire:thisSceneSim startingPoint:CGPointMake(enemyShipSim.position.x + theFire.size.width-enemyShipSim.frame.size.width/1.45, enemyShipSim.position.y)];
            theFire.physicsBody.categoryBitMask = weaponCategory;
            theFire.physicsBody.contactTestBitMask = userShipCategory;
            weaponEffect.emissionAngle = 0;
            [theFire addChild:weaponEffect];
            [WeaponsFire fireWeapon:1];
        }
        

    }else{
        if (timer) {
            [timer invalidate];
            SKEmitterNode * explosion = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle]pathForResource:@"Explosion" ofType:@"sks"]];
            SKAction * explosionSound = [SKAction playSoundFileNamed:@"explosion1.wav" waitForCompletion:NO];

            timeToKillLabel = [SKLabelNode labelNodeWithFontNamed: @"OratorStd"];
            timeToKillLabel.fontColor = [UIColor whiteColor];
            timeToKillLabel.fontSize = 32;
            timeToKillLabel.position = CGPointMake(thisSceneSim.frame.size.width/2, thisSceneSim.frame.size.height/1.2);
            timeToKillLabel.zPosition = 5;
            if (fireSec == 1) {
                timeToKillLabel.text = [NSString stringWithFormat:@"Kill Time: %d second",fireSec];
            }else{
                timeToKillLabel.text = [NSString stringWithFormat:@"Kill Time: %d seconds",fireSec];
            }
            switch (theSimType) {
                case 0:
                    explosion.position = CGPointMake(enemyShipSim.position.x, enemyShipSim.position.y);
                    [thisSceneSim addChild:explosion];
                    enemyShipSim.hidden = YES;
                    [thisSceneSim addChild:timeToKillLabel];
                    [thisSceneSim addChild:stopButton];
                    if (audioPOS == 0) {
                        [thisSceneSim runAction:explosionSound];
                    }
                    

                    break;
                case 1:
                    explosion.position = CGPointMake(userShipSim.position.x, userShipSim.position.y);
                    [thisSceneSim addChild:explosion];
                    userShipSim.hidden = YES;
                    [thisSceneSim addChild:timeToKillLabel];
                    [thisSceneSim addChild:stopButton];
                    if (audioPOS == 0) {
                        [thisSceneSim runAction:explosionSound];
                    }

                default:
                    break;
            }
        }
    }
}
-(BOOL)isConnected{
    BOOL isCon;
    Reachability * isReachable = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [isReachable currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        isCon = false;
        return isCon;
    }else{
        isCon = true;
        return isCon;
    }
}
- (void)showAlert:(NSString *)message {
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Connection Error" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

+(void)setShipSelectorPOS:(int)isOpen{
    shipSelectorOpen = isOpen;
}
+(void)tab1POS:(int)pos{
    tab1 = pos;
}
+(void)tab2POS:(int)pos{
    tab2 = pos;
}
+(void)tab3POS:(int)pos{
    tab3 = pos;
}
+(void)tab4POS:(int)pos{
    tab4 = pos;
}
+(void)hideView:(BOOL)isHidden{
    sideDock.hidden = isHidden;
}
+(int)getAudioPOS{
    return audioPOS;
}

@end
